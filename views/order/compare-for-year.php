<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="row">
    <div class="col-12">
        <h1>Сравнение показателей предыдущего периода</h1>
    </div>
    <hr>
    <div class="col-12">
        <?php $form = ActiveForm::begin(['action' => ['order/compare-for-year', 'method' => 'post'], 'id' => 'FormGeneratedOrderCompareForYear']); ?>
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-8 mt-md-0 mt-2">
                        <div class="row">
                            <div class="col-4 d-flex justify-content-center align-items-center">
                                <label class="text-center">Выберите период</label>
                            </div>
                            <div class="col-8 d-flex justify-content-center align-items-center">
                                <label for="">с</label>
                                <input type="date" class="form-control" name="date_start" value="<?= $dtStart->format("Y-m-d") ?>">
                                <label for="">по</label>
                                <input type="date" class="form-control" name="date_end" value="<?= $dtEnd->format("Y-m-d") ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-md-0 mt-2">
                        <select class="form-select" name="mode">
                            <?php if ($mode == "division") : ?>
                                <option value="division" selected>по дивизиону</option>
                                <option value="company">по предприятию</option>
                            <?php else : ?>
                                <option value="division">по дивизиону</option>
                                <option value="company" selected>по предприятию</option>
                            <?php endif ?>
                        </select>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-8 mt-md-0"></div>
                    <div class="col-md-4 mt-md-0 ">
                        <select class="form-select hidden" name="company">
                            <?php foreach ($objCompanies as $key => $obj) : ?>
                                <?php if ($key == 0) : ?>
                                    <option selected value="<?= $obj->id_company ?>"><?= $obj->company_name ?></option>
                                <?php else : ?>
                                    <option value="<?= $obj->id_company ?>"><?= $obj->company_name ?></option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2 mt-md-0 mt-2 d-flex flex-column">
                <button type="submit" class="btn btn-primary btn-submit">Сформировать</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div id="ChartCompareForYear"></div>
    </div>
</div>