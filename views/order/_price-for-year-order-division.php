<?php

use app\models\Product;

?>

<div class="row">

    <div class="col-12">
        <h1>По всему дивизиону</h1>
    </div>

    <div class="col-12">

        <table class="table table-bordered mt-3">
            <thead>
                <tr>
                    <th>Месяц</th>
                    <th>Артикул продукции</th>
                    <th>Наименование продукции</th>
                    <th>Сумма</th>
                </tr>
            </thead>

            <tbody>

                <?php


                $priceYear = 0;
                foreach ($dataTable as $m => $p) : ?>

                    <?php
                    $priceMonth = 0;
                    ?>
                    <?php foreach ($p as $id_product => $price) : ?>
                        <?php
                        $product = Product::findOne(['id_product' => $id_product]);
                        $priceMonth += $price;
                        ?>
                        <tr>
                            <td><?= $m ?></td>
                            <td><?= $product->articul ?></td>
                            <td><?= $product->caption ?></td>
                            <td><?= $price ?></td>
                        </tr>

                    <?php endforeach ?>

                    <?php
                    $priceYear += $priceMonth;
                    ?>
                    <tr>
                        <td colspan="3">Итого за месяц</td>
                        <td><?= $priceMonth ?></td>
                    </tr>
                <?php endforeach ?>
                <tr>
                    <td colspan="3">Итого за год</td>
                    <td><?= $priceYear ?></td>
                </tr>
            </tbody>
        </table>
    </div>

</div>