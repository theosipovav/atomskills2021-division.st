<?php

use app\models\Product;


?>
<div class="row">
    <div class="col-12">
        <h1>Отчет по произведенной продукции с <?= $dtStart->format("d-m-Y") ?> по <?= $dtEnd->format("d-m-Y") ?> по дивизиону</h1>
    </div>
    <div class="col-12">

        <table class="table table-bordered mt-3">
            <thead>
                <tr>
                    <th>Год</th>
                    <th>Месяц</th>
                    <th>Артикул продукции</th>
                    <th>Наименование продукции</th>
                    <th>Ед. изм.</th>
                    <th>Количество</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataTable as $year => $itemsYear) : ?>
                    <?php foreach ($itemsYear as $mouth => $itemsMouth) : ?>
                        <?php foreach ($itemsMouth as $id_product => $item) : ?>
                            <?php
                            $product = Product::findOne(['id_product' => $id_product]);
                            ?>
                            <tr>
                                <td><?= $year ?></td>
                                <td><?= $mouth ?></td>
                                <td><?= $product->articul ?></td>
                                <td><?= $product->caption ?></td>
                                <td><?= $item['unit'] ?></td>
                                <td><?= $item['quantity'] ?></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endforeach ?>
                <?php endforeach ?>

                <tr>
                    <td colspan="6">
                        Итоговые показатели:
                    </td>

                </tr>
                <?php foreach ($dataTableFinally as $id_product => $item) : ?>
                    <?php
                    $product = Product::findOne(['id_product' => $id_product]);
                    ?>
                    <tr>
                        <td colspan="2"><?= $product->articul ?></td>
                        <td colspan="2"><?= $product->caption ?></td>
                        <td><?= $item['unit'] ?></td>
                        <td><?= $item['quantity'] ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>


</div>