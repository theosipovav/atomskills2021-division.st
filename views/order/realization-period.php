<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="row">
    <div class="col-12">
        <h1>Отчет по произведенной продукции за период</h1>
    </div>
    <hr>
    <div class="col-12">
        <?php $form =  ActiveForm::begin(['action' => ['order/realization-period', 'method' => 'post'], 'id' => 'FormGeneratedOrderRealizationPeriod']); ?>
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-8 mt-md-0 mt-2">
                        <div class="row">
                            <div class="col-4 d-flex justify-content-center align-items-center">
                                <label class="text-center">Выберите период</label>
                            </div>
                            <div class="col-8 d-flex justify-content-center align-items-center">
                                <label for="">с</label>
                                <input type="date" class="form-control" name="date_start" value="<?= $dtStart->format("Y-m-d") ?>">
                                <label for="">по</label>
                                <input type="date" class="form-control" name="date_end" value="<?= $dtEnd->format("Y-m-d") ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-md-0 mt-2">
                        <select class="form-select" name="mode">
                            <?php if ($mode == "division") : ?>
                                <option value="division" selected>по дивизиону</option>
                                <option value="company">по предприятию</option>
                            <?php else : ?>
                                <option value="division">по дивизиону</option>
                                <option value="company" selected>по предприятию</option>
                            <?php endif ?>
                        </select>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-8 mt-md-0"></div>
                    <div class="col-md-4 mt-md-0 ">
                        <select class="form-select hidden" name="company">
                            <?php foreach ($objCompanies as $key => $obj) : ?>
                                <?php if ($key == 0) : ?>
                                    <option selected value="<?= $obj->id_company ?>"><?= $obj->company_name ?></option>
                                <?php else : ?>
                                    <option value="<?= $obj->id_company ?>"><?= $obj->company_name ?></option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2 mt-md-0 mt-2 d-flex flex-column">
                <button type="submit" class="btn btn-primary ">Сформировать</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php if ($mode == 'division') : ?>
    <div class="row">
        <?= $this->render(
            '_realization-period-division',
            [
                'dataTable' => $dataTable,
                'dataTableFinally' => $dataTableFinally,
                'dtStart' => $dtStart,
                'dtEnd' => $dtEnd,
            ]
        ) ?>
    </div>
<?php endif ?>
<?php if ($mode == 'company') : ?>
    <div class="row">
        <?= $this->render(
            '_realization-period-company',
            [
                'dataTable' => $dataTable,
                'dataTableFinally' => $dataTableFinally,
                'dtStart' => $dtStart,
                'dtEnd' => $dtEnd,
                'id_company' => $id_company
            ]
        )
        ?>
    </div>
<?php endif ?>