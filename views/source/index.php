<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список источников для REST API';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-index">

    <h1 class="mt-1 mb-3"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить новую запись', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{items}',
        'columns' => [
            'id_source',
            'value',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">{view}{update}{delete}{link}</div>',
                'contentOptions' =>
                [
                    'width' => '75px',
                ],
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="bi bi-eye-fill"></i>', $url, [
                            'class' => 'btn btn-primary',
                            'title' => 'Показать',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="bi bi-pencil-fill"></i>', $url, [
                            'class' => 'btn btn-primary',
                            'title' => 'Изменить',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="bi bi-trash-fill"></i>', $url, [
                            'class' => 'btn btn-primary',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>