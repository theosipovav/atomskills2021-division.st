<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <h1>Пользователи системы</h1>
    <p>
        <?= Html::a('Добавить пользователя ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{items}',
        'tableOptions' => [
            'class' => 'table table-control table-sm table-striped table-borderless table-hover table-manager-user'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'email:email',
            'last_name',
            'first_name',
            'second_name',
            'admin:boolean',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">{view}{update}{delete}{link}</div>',
                'contentOptions' =>
                [
                    'width' => '75px',
                ],
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="bi bi-eye-fill"></i>', $url, [
                            'class' => 'btn btn-primary',
                            'title' => 'Показать',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="bi bi-pencil-fill"></i>', $url, [
                            'class' => 'btn btn-primary',
                            'title' => 'Изменить',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="bi bi-trash-fill"></i>', $url, [
                            'class' => 'btn btn-primary',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],


        ],
    ]); ?>


</div>