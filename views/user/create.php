<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1>Создание нового пользователя</h1>
    <hr>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'username')->input('text') ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'passwordRepeat')->passwordInput() ?>
    <?= $form->field($model, 'lastName')->input('text') ?>
    <?= $form->field($model, 'firstName')->input('text') ?>
    <?= $form->field($model, 'secondName')->input('text') ?>
    <div class="form-group">
        <?= Html::submitButton('Создать аккаунт', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>