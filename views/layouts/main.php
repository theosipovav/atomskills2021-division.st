<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;
use yii\helpers\Url;

AppAsset::register($this);


$userActive = User::findOne(['id_user' => Yii::$app->user->id]);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <div class="container">
            <?= Alert::widget() ?>

            <div class="row">
                <div class="col-3">
                    <div class="">
                        <div class="d-flex flex-column block-name">
                            <?php


                            ?>
                            <p class="h-sup"><?= $userActive->last_name ?></p>
                            <p class="h-sup"><?= $userActive->first_name ?></p>
                            <p class="h-sup"><?= $userActive->second_name ?></p>
                        </div>
                        <hr>
                        <div class="d-flex flex-column block-role">
                            <?php if ($userActive->admin) : ?>
                                <p>Администратор</p>
                            <?php else : ?>
                                <p>Топ-менеджер </p>
                            <?php endif ?>
                            <a href="<?= Url::toRoute(['/site/logout']) ?>" class="btn btn-primary mt-2 mb-1" type="button">Выйти</a>
                            <a href="<?= Url::home() ?>" class="btn btn-primary" type="button">Главная</a>

                        </div>
                        <hr>
                    </div>
                    <div class="d-grid gap-2">
                        <?php if ($userActive->admin) : ?>
                            <a href="<?= Url::toRoute(['/source/']) ?>" class="btn btn-primary" type="button">Управление источниками</a>
                            <a href="<?= Url::toRoute(['/user/']) ?>" class="btn btn-primary" type="button">Управление пользователями</a>
                            <button id="ButtonUpdateAll" data-action="<?= Url::toRoute(['post/update-all']) ?>" class="btn btn-success">Обновить все данные</button>
                        <?php else : ?>
                            <a href="<?= Url::toRoute(['order/price-for-year']) ?>" class="btn btn-primary" type="button">Просмотреть стоимостные характеристики продукции за год</a>
                            <a href="<?= Url::toRoute(['order/realization-period']) ?>" class="btn btn-primary" type="button">Просмотр произведенной продукции за период</a>
                            <a href="<?= Url::toRoute(['order/compare-for-year']) ?>" class="btn btn-primary" type="button">Сравнить показатели предыдущего периода</a>
                        <?php endif ?>
                        <button id="ButtonUpdate" data-action="<?= Url::toRoute(['post/update-actual']) ?>" href="<?= Url::toRoute(['post/update-actual']) ?>" class="btn btn-primary" type="button">Обновить данные</button>
                    </div>

                </div>

                <div class="col-9">
                    <?= $content ?>
                </div>


            </div>





        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <p id="Msg"></p>
        </div>
    </footer>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>