<?php

use yii\helpers\Url;

?>

<div class="d-flex flex-column">
    <h2 class="h1 text-center mb-3">
        Добро пожаловать!
    </h2>
    <p class="text-center">
        Вас приветствует "Система производственных отчетов"
    </p>
    <p class="text-center">
        Для начало работы выберите необходимый модуль в боковом меню управление
    </p>
</div>