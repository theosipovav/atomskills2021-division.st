<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/*/* @var $model app\models\SignupForm */
/* @var $form ActiveForm */
?>
<div class="site-signup">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->input('text') ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'passwordRepeat')->passwordInput() ?>
    <?= $form->field($model, 'lastName')->input('text') ?>
    <?= $form->field($model, 'firstName')->input('text') ?>
    <?= $form->field($model, 'secondName')->input('text') ?>

    <div class="form-group">
        <?= Html::submitButton('Создать аккаунт', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-signup -->
</div>
</div>
</div>