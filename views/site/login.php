<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
?>


<main class="form-signin">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-login'],
        'fieldConfig' => [
            'template' => "{label}\n{input}\n{error}",
            'labelOptions' => ['class' => 'm-2 mb-1'],
        ],

    ]); ?>
    <h2 class="display-1 text-center">СПО "СТАРТ"</h2>
    <h1 class="h3 mb-3 fw-normal text-center">Пожалуйста, авторизуйтесь</h1>
    <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'id' => 'InputUsername', 'class' => 'form-control'])->label("Логин") ?>
    <?= $form->field($model, 'password')->passwordInput(['id' => 'InputPassword', 'class' => 'form-control'])->label("Пароль") ?>
    <?= $form->field($model, 'rememberMe')->checkbox([
        'template' => "<div class='d-flex justify-content-center align-items-center'><label>{input} Запомнить меня</labe></div>\n<div class='d-flex justify-content-center align-items-center'>{error}</div>",
    ]) ?>
    <div class="form-group">
        <div class="d-flex justify-content-center align-items-center">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</main>