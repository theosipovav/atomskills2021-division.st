<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 * @property string|null $password_reset_token Токен авторизации
 * @property string $email Электронная почта
 * @property int $status Статус
 * @property string $last_name Фамилия пользователя 
 * @property string|null $first_name Имя пользователя 
 * @property string|null $second_name Отчество пользователя 
 * @property string $created_at Дата создания
 * @property string $updated_at Дата обновления
 * @property float|null $role Роль пользователя
 * @property bool $admin Администратор
 * 
 * @property UserRole[] $userRoles 
 * 
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * Пользователь удален
     */
    const STATUS_DELETED = 0;
    /**
     * 
     */
    const STATUS_ACTIVE = 10;


    public static function tableName()
    {
        return 'user';
    }


    public static function findIdentity($id)
    {
        return static::findOne(['id_user' => $id, 'status' => self::STATUS_ACTIVE]);
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }


    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }


    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($auth_key)
    {
        return $this->getAuthKey() === $auth_key;
    }



    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }


    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }


    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }









    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'last_name'], 'required'],
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'last_name', 'first_name', 'second_name'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['username'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['admin'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Идентификатор пользователя',
            'username' => 'Логин',
            'auth_key' => 'Ключ авторизации',
            'password_hash' => 'Пароль',
            'password_reset_token' => 'Токен авторизации',
            'email' => 'Электронная почта',
            'status' => 'Статус',
            'last_name' => 'Фамилия пользователя',
            'first_name' => 'Имя пользователя',
            'second_name' => 'Отчество пользователя',
            'birthday' => 'Дата рождения пользователя',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'admin' => 'Администратор',
        ];
    }

    /**
     * Gets query for [[UserRoles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles()
    {
        return $this->hasMany(UserRole::className(), ['id_user' => 'id_user']);
    }
}
