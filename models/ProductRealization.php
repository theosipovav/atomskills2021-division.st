<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_realization".
 *
 * @property string|null $release_date Дата выпуска продукции
 * @property float|null $quantity Количество
 * @property int|null $id_company Идентификатор предприятия
 * @property string $created_at Дата создания записи
 * @property int $id_product_realization Идентификатор записи
 * @property int|null $id_unit Идентификатор едтницы измерения
 * @property int|null $id_product Идентификатор продукции
 * @property float|null $price Цена продукции
 *
 * @property Company $company
 * @property Product $product
 * @property Unit $unit
 */
class ProductRealization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_realization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['release_date', 'created_at'], 'safe'],
            [['quantity', 'price'], 'number'],
            [['id_company', 'id_unit', 'id_product'], 'default', 'value' => null],
            [['id_company', 'id_unit', 'id_product'], 'integer'],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['id_company' => 'id_company']],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['id_product' => 'id_product']],
            [['id_unit'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['id_unit' => 'id_unit']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'release_date' => 'Дата выпуска продукции',
            'quantity' => 'Количество',
            'id_company' => 'Идентификатор предприятия',
            'created_at' => 'Дата создания записи',
            'id_product_realization' => 'Идентификатор записи',
            'id_unit' => 'Идентификатор едтницы измерения',
            'id_product' => 'Идентификатор продукции',
            'price' => 'Цена продукции',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id_company' => 'id_company']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

    /**
     * Gets query for [[Unit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id_unit' => 'id_unit']);
    }
}
