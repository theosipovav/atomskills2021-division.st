<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property int $id_unit Идентификатор едтницы измерения
 * @property string|null $measure Сокращенное наименование ед. изм.
 * @property string $created_at Дата создания записи
 *
 * @property ProductRealization[] $productRealizations
 */
class Unit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['measure'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_unit' => 'Идентификатор едтницы измерения',
            'measure' => 'Сокращенное наименование ед. изм.',
            'created_at' => 'Дата создания записи',
        ];
    }

    /**
     * Gets query for [[ProductRealizations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductRealizations()
    {
        return $this->hasMany(ProductRealization::className(), ['id_unit' => 'id_unit']);
    }
}
