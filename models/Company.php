<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id_company Идентификатор предприятия
 * @property string|null $company_name Наименование предприятия
 * @property string|null $created_at Дата создания записи
 * @property string $entered Дата занесения информации о предприятии в систему
 * @property int|null $id_division Идентификатор дивизиона
 *
 * @property Division $division
 * @property ProductRealization[] $productRealizations
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_name'], 'string'],
            [['created_at', 'entered'], 'safe'],
            [['entered'], 'required'],
            [['id_division'], 'default', 'value' => null],
            [['id_division'], 'integer'],
            [['id_division'], 'exist', 'skipOnError' => true, 'targetClass' => Division::className(), 'targetAttribute' => ['id_division' => 'id_division']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_company' => 'Идентификатор предприятия',
            'company_name' => 'Наименование предприятия',
            'created_at' => 'Дата создания записи',
            'entered' => 'Дата занесения информации о предприятии в систему',
            'id_division' => 'Идентификатор дивизиона',
        ];
    }

    /**
     * Gets query for [[Division]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDivision()
    {
        return $this->hasOne(Division::className(), ['id_division' => 'id_division']);
    }

    /**
     * Gets query for [[ProductRealizations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductRealizations()
    {
        return $this->hasMany(ProductRealization::className(), ['id_company' => 'id_company']);
    }
}
