<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_division".
 *
 * @property int $id_user_division Идентификатор записи
 * @property int|null $id_user Идентификатор пользователя
 * @property int|null $id_division Идентификатор дивизиона
 *
 * @property Division $division
 * @property User $user
 */
class UserDivision extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_division';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_division'], 'default', 'value' => null],
            [['id_user', 'id_division'], 'integer'],
            [['id_division'], 'exist', 'skipOnError' => true, 'targetClass' => Division::className(), 'targetAttribute' => ['id_division' => 'id_division']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user_division' => 'Идентификатор записи',
            'id_user' => 'Идентификатор пользователя',
            'id_division' => 'Идентификатор дивизиона',
        ];
    }

    /**
     * Gets query for [[Division]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDivision()
    {
        return $this->hasOne(Division::className(), ['id_division' => 'id_division']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_user' => 'id_user']);
    }
}
