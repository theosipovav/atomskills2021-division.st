<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "division".
 *
 * @property int $id_division Идентификатор дивизиона
 * @property string $created_at Дата создания записи
 * @property string|null $division_name Наименование дивизиона
 *
 * @property Company[] $companies
 * @property Source[] $sources
 * @property UserDivision[] $userDivisions
 */
class Division extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'division';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['division_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_division' => 'Идентификатор дивизиона',
            'created_at' => 'Дата создания записи',
            'division_name' => 'Наименование дивизиона',
        ];
    }

    /**
     * Gets query for [[Companies]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id_division' => 'id_division']);
    }

    /**
     * Gets query for [[Sources]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSources()
    {
        return $this->hasMany(Source::className(), ['id_division' => 'id_division']);
    }

    /**
     * Gets query for [[UserDivisions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserDivisions()
    {
        return $this->hasMany(UserDivision::className(), ['id_division' => 'id_division']);
    }
}
