<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "source".
 *
 * @property int $id_source Идентификатор записи
 * @property string $value Значение параметра
 * @property int|null $id_division Идентификатор дивизиона
 *
 * @property Division $division
 */
class Source extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'source';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['id_division'], 'default', 'value' => null],
            [['id_division'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['id_division'], 'exist', 'skipOnError' => true, 'targetClass' => Division::className(), 'targetAttribute' => ['id_division' => 'id_division']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_source' => 'Идентификатор записи',
            'value' => 'Значение параметра',
            'id_division' => 'Идентификатор дивизиона',
        ];
    }

    /**
     * Gets query for [[Division]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDivision()
    {
        return $this->hasOne(Division::className(), ['id_division' => 'id_division']);
    }
}
