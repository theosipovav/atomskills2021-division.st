<?php

namespace app\models;

use DateTime;
use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $username;
    public $email;
    public $password;
    public $passwordRepeat;
    public $lastName;
    public $firstName;
    public $secondName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required', 'message' => 'Данное поля обязательно для заполнения'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Пользователь с таким логином уже существует'],
            ['username', 'string', 'min' => 2, 'max' => 255, 'message' => 'Максимальная длина поля 255 символов'],
            ['email', 'trim'],
            ['email', 'required', 'message' => 'Данное поля обязательно для заполнения'],
            ['email', 'email', 'message' => 'Данное поля не является действительным адресом электронной почты'],
            ['email', 'string', 'max' => 255, 'message' => 'Максимальная длина поля 255 символов'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Пользователь с такой электронной почтой уже существует'],
            ['password', 'required', 'message' => 'Данное поля обязательно для заполнения'],
            ['password', 'string', 'min' => 6, 'message' => 'Минимальная длина пароля 6 символов'],
            ['passwordRepeat', 'required', 'message' => 'Данное поля обязательно для заполнения'],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password', 'message' => "Пароли не совпадают"],
            ['lastName', 'trim'],
            ['lastName', 'required', 'message' => 'Данное поля обязательно для заполнения'],
            ['firstName', 'trim'],
            ['firstName', 'required', 'message' => 'Данное поля обязательно для заполнения'],
            ['secondName', 'trim'],
            ['secondName', 'required', 'message' => 'Данное поля обязательно для заполнения'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'passwordRepeat' => 'Повторите пароль',
            'email' => 'Электронная почта',
            'status' => 'Статус',
            'lastName' => 'Фамилия пользователя',
            'firstName' => 'Имя пользователя',
            'secondName' => 'Отчество пользователя',
        ];
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {


        if (!$this->validate()) {
            return null;
        }

        $dt = new DateTime();
        $user = new User();
        $user->username = $this->username;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->email = $this->email;
        $user->last_name = $this->lastName;
        $user->first_name = $this->firstName;
        $user->second_name = $this->secondName;
        $user->created_at = $dt->format('Y-m-d H:i:s');
        $user->updated_at = $dt->format('Y-m-d H:i:s');



        return $user->save(false) ? $user : null;
    }
}
