<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id_product Идентификатор продукции
 * @property string|null $articul Артикул продукции
 * @property string|null $caption Наименование продукции
 * @property string $created_at Дата создания записи
 *
 * @property ProductRealization[] $productRealizations
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['articul', 'caption'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_product' => 'Идентификатор продукции',
            'articul' => 'Артикул продукции',
            'caption' => 'Наименование продукции',
            'created_at' => 'Дата создания записи',
        ];
    }

    /**
     * Gets query for [[ProductRealizations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductRealizations()
    {
        return $this->hasMany(ProductRealization::className(), ['id_product' => 'id_product']);
    }
}
