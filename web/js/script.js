$(document).ready(function () {




    $("#ButtonUpdateAll").click(function (e) {
        e.preventDefault();
        var action = $(this).attr("data-action");

        $("#Msg").addClass("show");
        $("#Msg").html("Выполняется обновление данных. Пожалуйста, подождите");
        $.ajax({
            url: action,
            type: "POST",
            dataType: "json",
            success: function (res) {
                console.log(res);
                $("#Msg").html(res.data);
            },
            error: function (a, b, c) {
                // Форма отправлена с ошибкой
                alert("Возникла ошибка при обработке запроса. Пожалуйста, попробуйте повторить действие позднее.");
                console.log(a);
                console.log(b);
                console.log(c);
                $("#Msg").html("Возникла ошибка при обработке запроса. Пожалуйста, попробуйте повторить действие позднее.");
            },
        });

    });


    $("#ButtonUpdate").click(function (e) {

        e.preventDefault();
        var action = $(this).attr("data-action");
        $.ajax({
            url: action,
            type: "POST",
            dataType: "json",
            success: function (res) {
                $("#Msg").html(res.data);

            },
            error: function (a, b, c) {
                // Форма отправлена с ошибкой
                alert("Возникла ошибка при обработке запроса. Пожалуйста, попробуйте повторить действие позднее.");
                console.log(a);
                console.log(b);
                console.log(c);
            },
        });

    });







    if ($('#FormGeneratedOrderPriceForYear').length > 0) {
        var form = $('#FormGeneratedOrderPriceForYear');
        var selectMode = $(form).find("select[name=mode]");
        var selectCompany = $(form).find("select[name=company]");

        var valueSelectMode = $(selectMode).val();
        if (valueSelectMode == 'division') {
            $(selectCompany).addClass("hidden");
        } else {
            $(selectCompany).removeClass("hidden");
        }

        $(selectMode).change(function (e) {
            e.preventDefault();
            valueSelectMode = $(selectMode).val();
            if (valueSelectMode == 'division') {
                $(selectCompany).addClass("hidden");
            } else {
                $(selectCompany).removeClass("hidden");
            }

        });
    }


    if ($('#FormGeneratedOrderRealizationPeriod').length > 0) {
        var form = $('#FormGeneratedOrderRealizationPeriod');


        var selectMode = $(form).find("select[name=mode]");
        var selectCompany = $(form).find("select[name=company]");

        var valueSelectMode = $(selectMode).val();
        if (valueSelectMode == 'division') {
            $(selectCompany).addClass("hidden");
        } else {
            $(selectCompany).removeClass("hidden");
        }

        $(selectMode).change(function (e) {
            e.preventDefault();
            valueSelectMode = $(selectMode).val();
            if (valueSelectMode == 'division') {
                $(selectCompany).addClass("hidden");
            } else {
                $(selectCompany).removeClass("hidden");
            }

        });
    }



    if ($('#FormGeneratedOrderCompareForYear').length > 0) {
        var form = $('#FormGeneratedOrderCompareForYear');
        var selectMode = $(form).find("select[name=mode]");
        var selectCompany = $(form).find("select[name=company]");
        var valueSelectMode = $(selectMode).val();
        if (valueSelectMode == 'division') {
            $(selectCompany).addClass("hidden");
        } else {
            $(selectCompany).removeClass("hidden");
        }
        $(selectMode).change(function (e) {
            e.preventDefault();
            valueSelectMode = $(selectMode).val();
            if (valueSelectMode == 'division') {
                $(selectCompany).addClass("hidden");
            } else {
                $(selectCompany).removeClass("hidden");
            }
        });

        $('#FormGeneratedOrderCompareForYear').on("beforeSubmit", function () {
            let formSerialize = $(this).serialize();
            let action = $(this).attr("action");
            console.log(action);
            $.ajax({
                url: action,
                type: "POST",
                data: formSerialize,
                dataType: "json",
                success: function (res) {

                    var categories = [];
                    var datePrice = [];
                    var datePricePre = [];
                    res.forEach(item => {
                        console.log(item);
                        categories.push(item.date);
                        datePrice.push(item.price);
                        datePricePre.push(item.price_pre);
                    });


                    var options = {
                        series: [
                            {
                                name: 'Текущий период',
                                data: datePrice
                            }, {
                                name: 'Предыдущий период',
                                data: datePricePre
                            },
                        ],
                        chart: {
                            type: 'bar',
                            height: 350
                        },
                        plotOptions: {
                            bar: {
                                horizontal: false,
                                columnWidth: '55%',
                                endingShape: 'rounded'
                            },
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            show: true,
                            width: 2,
                            colors: ['transparent']
                        },
                        xaxis: {
                            categories: categories,
                        },
                        yaxis: {
                            title: {
                                text: 'выручка'
                            }
                        },
                        fill: {
                            opacity: 1
                        },
                        tooltip: {
                            y: {
                                formatter: function (val) {
                                    return "$ " + val + " thousands"
                                }
                            }
                        }
                    };

                    var chart = new ApexCharts(document.querySelector("#ChartCompareForYear"), options);
                    chart.render();
                },
                error: function (a, b, c) {
                    // Форма отправлена с ошибкой
                    alert("Возникла ошибка при обработке запроса. Пожалуйста, попробуйте повторить действие позднее.");
                    console.log(a);
                    console.log(b);
                    console.log(c);
                },
            });


            return false;
        });

        var selectMode = $(form).find("select[name=mode]");
        var selectCompany = $(form).find("select[name=company]");

        var valueSelectMode = $(selectMode).val();
        if (valueSelectMode == 'division') {
            $(selectCompany).addClass("hidden");
        } else {
            $(selectCompany).removeClass("hidden");
        }

        $(selectMode).change(function (e) {
            e.preventDefault();
            valueSelectMode = $(selectMode).val();
            if (valueSelectMode == 'division') {
                $(selectCompany).addClass("hidden");
            } else {
                $(selectCompany).removeClass("hidden");
            }

        });


    }
});

