<?php

namespace app\controllers;

use app\models\Company;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Product;
use app\models\ProductRealization;
use app\models\Role;
use app\models\SignupForm;
use app\models\Source;
use app\models\Unit;
use app\models\User;
use DateTime;
use OrderDivisionYearTable;
use yii\helpers\Url;

class OrderController extends Controller
{

    public function actionPriceForYear()
    {
        $age = (new DateTime("now"))->format('Y');
        $mode = '';
        $dataTable = array();
        $objCompanies = Company::find()->all();
        $id_company = -1;


        if (Yii::$app->request->isPost) {


            $dataPost = Yii::$app->request->post();




            $age = $dataPost['age'];
            $mode = $dataPost['mode'];

            if ($mode == 'division') {


                for ($i = 1; $i <= 12; $i++) {
                    $dataTable[$i] = array();
                    $dtStart = new DateTime("$age-$i-01");
                    $dtEnd = new DateTime("$age-$i-31");
                    $productsRealization = ProductRealization::find()
                        ->where(['>=', 'release_date',  $dtStart->format("Y-m-d")])
                        ->andWhere(['<=', 'release_date',  $dtEnd->format("Y-m-d")])
                        ->all();
                    foreach ($productsRealization as $value) {

                        $id_product = $value['id_product'];
                        $price = $value['quantity'] * $value['price'];

                        if (empty($dataTable[$i][$id_product])) {
                            $dataTable[$i][$id_product] = $price;
                        } else {
                            $dataTable[$i][$id_product] += $price;
                        }
                    }
                }
            } else {
                $id_company =  $dataPost['company'];


                for ($i = 1; $i <= 12; $i++) {
                    $dataTable[$i] = array();
                    $dtStart = new DateTime("$age-$i-01");
                    $dtEnd = new DateTime("$age-$i-31");
                    $productsRealization = ProductRealization::find()
                        ->where(['>=', 'release_date',  $dtStart->format("Y-m-d")])
                        ->andWhere(['<=', 'release_date',  $dtEnd->format("Y-m-d")])
                        ->andWhere(['id_company' => $id_company])
                        ->all();
                    foreach ($productsRealization as $value) {
                        $id_product = $value['id_product'];
                        $price = $value['quantity'] * $value['price'];

                        if (empty($dataTable[$i][$id_product])) {
                            $dataTable[$i][$id_product] = $price;
                        } else {
                            $dataTable[$i][$id_product] += $price;
                        }
                    }
                }
            }
        }



        return $this->render("price-for-year.php", [
            'mode' => $mode,
            'dataTable' => $dataTable,
            'age' => $age,
            'objCompanies' => $objCompanies,
            'id_company' => $id_company,
        ]);
    }


    public function actionRealizationPeriod()
    {
        $dtStart = (new DateTime("now"));
        $dtEnd = (new DateTime("now"));
        $mode = '';
        $objCompanies = Company::find()->all();
        $id_company = -1;
        $dataTable = array();
        $dataTableFinally = array();

        if (Yii::$app->request->isPost) {
            $dataPost = Yii::$app->request->post();
            $dtStart = new DateTime($dataPost['date_start']);
            $dtEnd = new DateTime($dataPost['date_end']);
            $mode = $dataPost['mode'];
            if ($mode == "division") {
                for ($year = intval($dtStart->format('Y')); $year <= intval($dtEnd->format('Y')); $year++) {
                    $dataTable[$year] = array();
                    for ($i = 1; $i <= 12; $i++) {
                        $dtStartSearch = (new DateTime("$year-$i-01"));
                        $dtEndSearch = (new DateTime("$year-12-31"));
                        if ($dtStartSearch > $dtEnd) {
                            break;
                        }
                        $dataTable[$year][$i] = array();

                        $productsRealization = ProductRealization::find()
                            ->where(['>=', 'release_date',  $dtStartSearch->format("Y-m-d")])
                            ->andWhere(['<=', 'release_date',  $dtEndSearch->format("Y-m-d")])
                            ->all();
                        foreach ($productsRealization as $value) {
                            $id_product = $value['id_product'];
                            $quantity = $value['quantity'];
                            $unit = Unit::findOne(['id_unit' => $value['id_unit']]);
                            if (empty($dataTable[$year][$i][$id_product])) {
                                $dataTable[$year][$i][$id_product]['quantity'] = $quantity;
                                $dataTable[$year][$i][$id_product]['unit'] = $unit->measure;
                            } else {
                                $dataTable[$year][$i][$id_product]['quantity'] += $quantity;
                                $dataTable[$year][$i][$id_product]['unit'] = $unit->measure;
                            }

                            if (empty($dataTableFinally[$id_product])) {
                                $dataTableFinally[$id_product]['quantity'] = $quantity;
                                $dataTableFinally[$id_product]['unit'] = $unit->measure;
                            } else {
                                $dataTableFinally[$id_product]['quantity'] += $quantity;
                                $dataTableFinally[$id_product]['unit'] = $unit->measure;
                            }
                        }
                    }
                }
            } else {
                $id_company =  $dataPost['company'];
                for ($year = intval($dtStart->format('Y')); $year <= intval($dtEnd->format('Y')); $year++) {
                    $dataTable[$year] = array();
                    for ($i = 1; $i <= 12; $i++) {
                        $dtStartSearch = (new DateTime("$year-$i-01"));
                        $dtEndSearch = (new DateTime("$year-12-31"));
                        if ($dtStartSearch > $dtEnd) {
                            break;
                        }
                        $dataTable[$year][$i] = array();

                        $productsRealization = ProductRealization::find()
                            ->where(['>=', 'release_date',  $dtStartSearch->format("Y-m-d")])
                            ->andWhere(['<=', 'release_date',  $dtEndSearch->format("Y-m-d")])
                            ->andWhere(['id_company' => $id_company])
                            ->all();
                        foreach ($productsRealization as $value) {
                            $id_product = $value['id_product'];
                            $quantity = $value['quantity'];
                            $unit = Unit::findOne(['id_unit' => $value['id_unit']]);
                            if (empty($dataTable[$year][$i][$id_product])) {
                                $dataTable[$year][$i][$id_product]['quantity'] = $quantity;
                                $dataTable[$year][$i][$id_product]['unit'] = $unit->measure;
                            } else {
                                $dataTable[$year][$i][$id_product]['quantity'] += $quantity;
                                $dataTable[$year][$i][$id_product]['unit'] = $unit->measure;
                            }

                            if (empty($dataTableFinally[$id_product])) {
                                $dataTableFinally[$id_product]['quantity'] = $quantity;
                                $dataTableFinally[$id_product]['unit'] = $unit->measure;
                            } else {
                                $dataTableFinally[$id_product]['quantity'] += $quantity;
                                $dataTableFinally[$id_product]['unit'] = $unit->measure;
                            }
                        }
                    }
                }
            }
        }







        return $this->render(
            'realization-period.php',
            [
                'mode' => $mode,
                'id_company' => $id_company,
                'objCompanies' => $objCompanies,
                'dtStart' => $dtStart,
                'dtEnd' => $dtEnd,
                'dataTable' => $dataTable,
                'dataTableFinally' => $dataTableFinally
            ]
        );
    }


    public function actionCompareForYear()
    {
        $dtStart = (new DateTime("now"));
        $dtEnd = (new DateTime("now"));
        $mode = '';
        $objCompanies = Company::find()->all();
        $id_company = -1;
        $dataTable = array();
        if (Yii::$app->request->isPost) {
            $dataPost = Yii::$app->request->post();



            $dtStart = new DateTime($dataPost['date_start']);
            $dtEnd = new DateTime($dataPost['date_end']);
            $mode = $dataPost['mode'];
            $res = array();
            for ($year = intval($dtStart->format('Y')); $year <= intval($dtEnd->format('Y')); $year++) {
                for ($mouth = intval($dtStart->format('m')); $mouth <= 12; $mouth++) {


                    $releaseDateStart = (new DateTime("$year-$mouth-01"));
                    $releaseDateEnd = (new DateTime("$year-$mouth-31"));


                    $releaseDateStartPrev = (new DateTime("$year-$mouth-01"))->modify("-1 year");
                    $releaseDateEndPrev = (new DateTime("$year-$mouth-31"))->modify("-1 year");

                    if ($releaseDateStart > $dtEnd) {
                        break;
                    }

                    $arr = array();

                    if ($mode == "division") {
                        $productsRealization = ProductRealization::find()
                            ->where(['>=', 'release_date',  $releaseDateStart->format("Y-m-d")])
                            ->andWhere(['<=', 'release_date',  $releaseDateEnd->format("Y-m-d")])
                            ->orderBy('release_date DESC')
                            ->all();

                        $productsRealizationPrev = ProductRealization::find()
                            ->where(['>=', 'release_date',  $releaseDateStartPrev->format("Y-m-d")])
                            ->andWhere(['<=', 'release_date', $releaseDateEndPrev->format("Y-m-d")])
                            ->orderBy('release_date DESC')
                            ->all();
                    } else {

                        $id_company =  $dataPost['company'];
                        $productsRealization = ProductRealization::find()
                            ->where(['>=', 'release_date',  $releaseDateStart->format("Y-m-d")])
                            ->andWhere(['<=', 'release_date',  $releaseDateEnd->format("Y-m-d")])
                            ->andWhere(['id_company' => $id_company])
                            ->orderBy('release_date DESC')
                            ->all();

                        $productsRealizationPrev = ProductRealization::find()
                            ->where(['>=', 'release_date',  $releaseDateStartPrev->format("Y-m-d")])
                            ->andWhere(['<=', 'release_date', $releaseDateEndPrev->format("Y-m-d")])
                            ->andWhere(['id_company' => $id_company])
                            ->orderBy('release_date DESC')
                            ->all();
                    }
                    $arr['date'] = "$mouth-$year";
                    $arr['price'] = 0;
                    $arr['price_pre'] = 0;
                    foreach ($productsRealization as $key => $p) {
                        $arr['price'] += $p->quantity * $p->price;
                    }
                    foreach ($productsRealizationPrev as $key => $p) {
                        $arr['price_pre'] += $p->quantity * $p->price;
                    }

                    array_push($res, $arr);
                }
            }
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $this->asJson($res);
        } else {
            return $this->render(
                'compare-for-year',
                [
                    'mode' => $mode,
                    'id_company' => $id_company,
                    'objCompanies' => $objCompanies,
                    'dtStart' => $dtStart,
                    'dtEnd' => $dtEnd,
                    'dataTable' => $dataTable,
                ]
            );
        }
    }
}
