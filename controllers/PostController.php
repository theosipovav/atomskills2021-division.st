<?php

namespace app\controllers;

use app\models\Company;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Product;
use app\models\ProductRealization;
use app\models\Role;
use app\models\SignupForm;
use app\models\Source;
use app\models\Unit;
use app\models\User;
use DateTime;
use yii\helpers\Url;

class PostController extends Controller
{

    public $errors = array();


    /**
     * Добавить новые данные
     */
    public function actionUpdateActual()
    {
        $lastProductRealization = ProductRealization::find()->orderBy(['release_date' => SORT_DESC])->one();



        $sources = Source::find()->all();
        $productsByFactories = array();


        $companies = Company::find()->all();

        foreach ($sources as $source) {
            $ip = $source->value;
            $url = "http://" . $ip . "";

            foreach ($companies as $key => $company) {
                $dt = new DateTime($lastProductRealization->release_date);
                $dt->modify('+1 day');
                $dtNow = new DateTime("now");

                while ($dt < $dtNow) {
                    $dateString = $dt->format("Y-m-d");
                    $id_company = $company->id_company;
                    // Получить информацию по конкретному предприятию (:factoryId) и список произведенной в (:timeKey) продукции.
                    $urlForFactoryDetail = $url . "/factories/" . $id_company . "/" . $dateString;
                    $factoriesDetailProducts = $this->getRestData($urlForFactoryDetail);

                    $isError = false;
                    if ($company->company_name != $factoriesDetailProducts['factoryCaption']) {
                        $isError = true;
                    }
                    if ($isError) {
                        $errorsDetail = array();
                        array_push($errorsDetail, "Нарушена целостность структуры данных");
                        array_push($errorsDetail, $company->id_company . " " . $company->company_name . " " . $company->entered);
                        array_push($errorsDetail, $factoriesDetailProducts['key'] . " " . $factoriesDetailProducts['factoryId'] . " " . $factoriesDetailProducts['factoryCaption']);
                        array_push($this->errors, $errorsDetail);
                        continue;
                    }


                    $this->updateProductRealization($factoriesDetailProducts['products'], $id_company, $dateString, $productsByFactories, true);
                    $dt->modify('+1 day');
                }
            }
        }

        $res = array();


        if (count($this->errors) > 0) {
            $res['status'] = "e";
            $res['data'] = "При обновление данных обнаружены ошибки";
        } else {

            $res['status'] = "s";
            $res['data'] = "Данные успешно обновлены";
        }


        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $this->asJson($res);
    }






    /**
     * Возвращает список предприятий
     */
    public function actionUpdateAll()
    {

        $units = array();


        ProductRealization::deleteAll();
        Company::deleteAll();
        Product::deleteAll();
        Unit::deleteAll();

        $sources = Source::find()->all();

        $factories = null;
        $productsByFactories = array();


        foreach ($sources as $source) {
            $ip = $source->value;
            $url = "http://" . $ip . "";
            // Получить более детализированную информацию по конкретному предприятию (:factoryId) и список выпускаемой им продукции
            $urlForFactories = $url . "/factories";
            $factories = $this->getRestData($urlForFactories);
            $this->updateCompanyFroDb($factories, 1);
            foreach ($factories as $factory) {
                $id_company = $factory['id'];
                //  Получить информацию по конкретному предприятию (:factoryId) и список произведенной в (:timeKey) продукции.
                $urlForFactoryDetail = $url . "/factories/" .  $id_company;
                $factoriesDetail = $this->getRestData($urlForFactoryDetail);
                array_push($productsByFactories, array($id_company => $factoriesDetail));
                $isError = false;
                if ($factory['caption'] != $factoriesDetail['caption']) {
                    $isError = true;
                }
                if ($factory['entered'] != $factoriesDetail['entered']) {
                    $isError = true;
                }
                if ($isError) {
                    $errorsDetail = array();
                    array_push($errorsDetail, "Нарушена целостность структуры данных");
                    array_push($errorsDetail, $factory['id'] . " " . $factory['caption'] . " " . $factory['entered']);
                    array_push($errorsDetail, $factoriesDetail['id'] . " " . $factoriesDetail['caption'] . " " . $factoriesDetail['entered']);
                    array_push($this->errors, $errorsDetail);
                    continue;
                }
                foreach ($factoriesDetail['products'] as $key => $value) {
                    $this->updateUnits($value['measure']);
                }
                $this->updateProductsForDb($factory['id'], $factoriesDetail['products']);
            }
            foreach ($factories as $key => $factory) {
                $dt = new DateTime("2020-01-01");
                $dtNow = new DateTime("now");
                while ($dt < $dtNow) {
                    $dateString = $dt->format("Y-m-d");
                    $id = $factory['id'];
                    // Получить информацию по конкретному предприятию (:factoryId) и список произведенной в (:timeKey) продукции.
                    $urlForFactoryDetail = $url . "/factories/" . $id . "/" . $dateString;
                    $factoriesDetailProducts = $this->getRestData($urlForFactoryDetail);
                    $id_company = $factory['id'];
                    $isError = false;
                    if ($factory['caption'] != $factoriesDetailProducts['factoryCaption']) {
                        $isError = true;
                    }
                    if ($isError) {
                        $errorsDetail = array();
                        array_push($errorsDetail, "Нарушена целостность структуры данных");
                        array_push($errorsDetail, $factory['id'] . " " . $factory['caption'] . " " . $factory['entered']);
                        array_push($errorsDetail, $factoriesDetailProducts['key'] . " " . $factoriesDetailProducts['factoryId'] . " " . $factoriesDetailProducts['factoryCaption']);
                        array_push($this->errors, $errorsDetail);
                        continue;
                    }
                    $this->updateProductRealization($factoriesDetailProducts['products'], $id_company, $dateString, $productsByFactories);
                    $dt->modify('+1 day');
                }
            }
        }

        $res = array();


        if (count($this->errors) > 0) {
            $res['status'] = "e";
            $res['data'] = "При обновление данных обнаружены ошибки";
        } else {

            $res['status'] = "s";
            $res['data'] = "Данные успешно обновлены";
        }


        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $this->asJson($res);
    }





    private function getRestData($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }



    private function updateUnits($value)
    {
        $unit = Unit::findOne(["measure" => $value]);
        if ($unit == null) {
            $unit = new Unit();
            $unit->measure = $value;
            $unit->save();
        }
    }

    private function updateCompanyFroDb($factories, $id_division = 1)
    {
        foreach ($factories as $factory) {
            $company = new Company();
            $company->id_company = $factory['id'];
            $company->company_name = $factory['caption'];
            $company->entered = $factory['entered'];
            $company->id_division = $id_division;
            $company->save();
        }
    }

    private function updateProductsForDb($id_company, $data_products)
    {
        foreach ($data_products as $value) {
            $product = new Product();
            $product->id_product = $id_company . $value['id'];
            $product->articul = $value['articul'];
            $product->caption = $value['caption'];
            $product->save();
        }
    }

    private function updateProductRealization($data_products, $id_company, $release_date, $productsByFactories, $isCreateProduct = false)
    {
        foreach ($data_products as $value) {
            $productRealization = new ProductRealization();
            $productRealization->quantity = $value['qty'];
            $productRealization->id_company = $id_company;
            $productRealization->release_date = $release_date;
            if ($isCreateProduct) {
                $product = Product::find()->where(['id_product' => $id_company . $value['id']])->one();
                if ($product == null) {
                    $product = new Product();
                    $product->id_product = $id_company . $value['id'];
                    $product->articul = $value['articul'];
                    $product->caption = $value['caption'];
                    $product->save();
                }
            } else {
                $productRealization->id_product = $id_company . $value['id'];
            }


            $isBreak = false;
            $priceValue = 0;
            foreach ($productsByFactories as $productByFactories) {
                if ($isBreak) {
                    break;
                }

                if ($productByFactories[$id_company]['products'] == null) {
                    continue;
                }
                if (empty($productByFactories[$id_company]['products'])) {
                    continue;
                }

                foreach ($productByFactories[$id_company]['products'] as $p) {
                    if ($p['id'] == $value['id']) {
                        $priceValue = $p['price'];
                        $isBreak = true;
                    }
                    if ($isBreak) {
                        break;
                    }
                }
            }
            $productRealization->price = $priceValue;
            $unit = Unit::findOne(['measure' => $value['measure']]);
            if ($unit == null) {
                $errorsDetail = array();
                array_push($errorsDetail, "Нарушена целостность структуры данных");
                array_push($errorsDetail, $value['id'] . " " . $value['caption'] . " " . $value['articul'] . $value['qty']);
                array_push($errorsDetail, "Единица измерения");
                array_push($this->errors, $errorsDetail);
                continue;
            }
            $productRealization->id_unit = $unit->id_unit;
            $productRealization->save(false);
        }
    }
}
