PGDMP     6    %                y            test    13.2    13.2 k               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    16396    test    DATABASE     a   CREATE DATABASE test WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Russian_Russia.1251';
    DROP DATABASE test;
                admin    false                       0    0    DATABASE test    COMMENT     L   COMMENT ON DATABASE test IS 'Сервер для тестирования';
                   admin    false    3096            �            1259    16757    company_id_seq    SEQUENCE     w   CREATE SEQUENCE public.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.company_id_seq;
       public          admin    false            �            1259    16517    company    TABLE       CREATE TABLE public.company (
    id_company integer DEFAULT nextval('public.company_id_seq'::regclass) NOT NULL,
    company_name text,
    created_at timestamp(0) without time zone DEFAULT now(),
    entered timestamp(0) without time zone NOT NULL,
    id_division integer
);
    DROP TABLE public.company;
       public         heap    admin    false    210                       0    0    TABLE company    COMMENT     J   COMMENT ON TABLE public.company IS 'Список предприятий';
          public          admin    false    202                       0    0    COLUMN company.id_company    COMMENT     d   COMMENT ON COLUMN public.company.id_company IS 'Идентификатор предприятия';
          public          admin    false    202                       0    0    COLUMN company.company_name    COMMENT     d   COMMENT ON COLUMN public.company.company_name IS 'Наименование предприятия';
          public          admin    false    202                       0    0    COLUMN company.created_at    COMMENT     Y   COMMENT ON COLUMN public.company.created_at IS 'Дата создания записи';
          public          admin    false    202                       0    0    COLUMN company.entered    COMMENT     �   COMMENT ON COLUMN public.company.entered IS 'Дата занесения информации о предприятии в систему';
          public          admin    false    202                       0    0    COLUMN company.id_division    COMMENT     a   COMMENT ON COLUMN public.company.id_division IS 'Идентификатор дивизиона';
          public          admin    false    202            �            1259    16760    division_id_seq    SEQUENCE     x   CREATE SEQUENCE public.division_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.division_id_seq;
       public          admin    false            �            1259    16700    division    TABLE     �   CREATE TABLE public.division (
    id_division integer DEFAULT nextval('public.division_id_seq'::regclass) NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    division_name text
);
    DROP TABLE public.division;
       public         heap    admin    false    211                        0    0    TABLE division    COMMENT     8   COMMENT ON TABLE public.division IS 'Дивизион';
          public          admin    false    208            !           0    0    COLUMN division.id_division    COMMENT     b   COMMENT ON COLUMN public.division.id_division IS 'Идентификатор дивизиона';
          public          admin    false    208            "           0    0    COLUMN division.created_at    COMMENT     Z   COMMENT ON COLUMN public.division.created_at IS 'Дата создания записи';
          public          admin    false    208            #           0    0    COLUMN division.division_name    COMMENT     b   COMMENT ON COLUMN public.division.division_name IS 'Наименование дивизиона';
          public          admin    false    208            �            1259    16530 	   id_config    SEQUENCE     r   CREATE SEQUENCE public.id_config
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
     DROP SEQUENCE public.id_config;
       public          postgres    false            �            1259    16766    id_product_realization_seq    SEQUENCE     �   CREATE SEQUENCE public.id_product_realization_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.id_product_realization_seq;
       public          admin    false            �            1259    16763    product_id_seq    SEQUENCE     w   CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.product_id_seq;
       public          admin    false            �            1259    16539    product    TABLE     �   CREATE TABLE public.product (
    id_product integer DEFAULT nextval('public.product_id_seq'::regclass) NOT NULL,
    articul character varying,
    caption text,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public.product;
       public         heap    admin    false    212            $           0    0    TABLE product    COMMENT     9   COMMENT ON TABLE public.product IS 'Продукция';
          public          admin    false    205            %           0    0    COLUMN product.id_product    COMMENT     `   COMMENT ON COLUMN public.product.id_product IS 'Идентификатор продукции';
          public          admin    false    205            &           0    0    COLUMN product.articul    COMMENT     Q   COMMENT ON COLUMN public.product.articul IS 'Артикул продукции';
          public          admin    false    205            '           0    0    COLUMN product.caption    COMMENT     [   COMMENT ON COLUMN public.product.caption IS 'Наименование продукции';
          public          admin    false    205            (           0    0    COLUMN product.created_at    COMMENT     Y   COMMENT ON COLUMN public.product.created_at IS 'Дата создания записи';
          public          admin    false    205            �            1259    16599    product_realization    TABLE     �  CREATE TABLE public.product_realization (
    release_date timestamp(0) without time zone,
    quantity numeric,
    id_company integer,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    id_product_realization integer DEFAULT nextval('public.id_product_realization_seq'::regclass) NOT NULL,
    id_unit integer,
    id_product integer,
    price double precision
);
 '   DROP TABLE public.product_realization;
       public         heap    admin    false    213            )           0    0    TABLE product_realization    COMMENT        COMMENT ON TABLE public.product_realization IS 'Товарный выпуск продукции по предприятию';
          public          admin    false    207            *           0    0 '   COLUMN product_realization.release_date    COMMENT     k   COMMENT ON COLUMN public.product_realization.release_date IS 'Дата выпуска продукции';
          public          admin    false    207            +           0    0 #   COLUMN product_realization.quantity    COMMENT     Q   COMMENT ON COLUMN public.product_realization.quantity IS 'Количество';
          public          admin    false    207            ,           0    0 %   COLUMN product_realization.id_company    COMMENT     p   COMMENT ON COLUMN public.product_realization.id_company IS 'Идентификатор предприятия';
          public          admin    false    207            -           0    0 %   COLUMN product_realization.created_at    COMMENT     e   COMMENT ON COLUMN public.product_realization.created_at IS 'Дата создания записи';
          public          admin    false    207            .           0    0 1   COLUMN product_realization.id_product_realization    COMMENT     r   COMMENT ON COLUMN public.product_realization.id_product_realization IS 'Идентификатор записи';
          public          admin    false    207            /           0    0 "   COLUMN product_realization.id_unit    COMMENT     x   COMMENT ON COLUMN public.product_realization.id_unit IS 'Идентификатор едтницы измерения';
          public          admin    false    207            0           0    0 %   COLUMN product_realization.id_product    COMMENT     l   COMMENT ON COLUMN public.product_realization.id_product IS 'Идентификатор продукции';
          public          admin    false    207            1           0    0     COLUMN product_realization.price    COMMENT     U   COMMENT ON COLUMN public.product_realization.price IS 'Цена продукции';
          public          admin    false    207            �            1259    16521    source    TABLE     �   CREATE TABLE public.source (
    id_source integer DEFAULT nextval('public.id_config'::regclass) NOT NULL,
    value character varying(255) NOT NULL,
    id_division integer
);
    DROP TABLE public.source;
       public         heap    postgres    false    204            2           0    0    TABLE source    COMMENT     C   COMMENT ON TABLE public.source IS 'Источник данных';
          public          postgres    false    203            3           0    0    COLUMN source.id_source    COMMENT     X   COMMENT ON COLUMN public.source.id_source IS 'Идентификатор записи';
          public          postgres    false    203            4           0    0    COLUMN source.value    COMMENT     P   COMMENT ON COLUMN public.source.value IS 'Значение параметра';
          public          postgres    false    203            5           0    0    COLUMN source.id_division    COMMENT     `   COMMENT ON COLUMN public.source.id_division IS 'Идентификатор дивизиона';
          public          postgres    false    203            �            1259    16769    unit_id_seq    SEQUENCE     t   CREATE SEQUENCE public.unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.unit_id_seq;
       public          admin    false            �            1259    16556    unit    TABLE     �   CREATE TABLE public.unit (
    id_unit integer DEFAULT nextval('public.unit_id_seq'::regclass) NOT NULL,
    measure character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public.unit;
       public         heap    admin    false    214            6           0    0 
   TABLE unit    COMMENT     E   COMMENT ON TABLE public.unit IS 'Единица измерения';
          public          admin    false    206            7           0    0    COLUMN unit.id_unit    COMMENT     i   COMMENT ON COLUMN public.unit.id_unit IS 'Идентификатор едтницы измерения';
          public          admin    false    206            8           0    0    COLUMN unit.measure    COMMENT     j   COMMENT ON COLUMN public.unit.measure IS 'Сокращенное наименование ед. изм.';
          public          admin    false    206            9           0    0    COLUMN unit.created_at    COMMENT     V   COMMENT ON COLUMN public.unit.created_at IS 'Дата создания записи';
          public          admin    false    206            �            1259    16451    user    TABLE     �  CREATE TABLE public."user" (
    id_user integer NOT NULL,
    username character varying(255) NOT NULL,
    auth_key character varying(32) NOT NULL,
    password_hash character varying(255) NOT NULL,
    password_reset_token character varying(255),
    email character varying(255) NOT NULL,
    status smallint DEFAULT 10 NOT NULL,
    last_name character varying(255) NOT NULL,
    first_name character varying(255),
    second_name character varying(255),
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    updated_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    admin boolean DEFAULT false NOT NULL
);
    DROP TABLE public."user";
       public         heap    admin    false            :           0    0    TABLE "user"    COMMENT     >   COMMENT ON TABLE public."user" IS 'Пользователи';
          public          admin    false    201            ;           0    0    COLUMN "user".id_user    COMMENT     b   COMMENT ON COLUMN public."user".id_user IS 'Идентификатор пользователя';
          public          admin    false    201            <           0    0    COLUMN "user".username    COMMENT     :   COMMENT ON COLUMN public."user".username IS 'Логин';
          public          admin    false    201            =           0    0    COLUMN "user".auth_key    COMMENT     O   COMMENT ON COLUMN public."user".auth_key IS 'Ключ авторизации';
          public          admin    false    201            >           0    0    COLUMN "user".password_hash    COMMENT     A   COMMENT ON COLUMN public."user".password_hash IS 'Пароль';
          public          admin    false    201            ?           0    0 "   COLUMN "user".password_reset_token    COMMENT     ]   COMMENT ON COLUMN public."user".password_reset_token IS 'Токен авторизации';
          public          admin    false    201            @           0    0    COLUMN "user".email    COMMENT     N   COMMENT ON COLUMN public."user".email IS 'Электронная почта';
          public          admin    false    201            A           0    0    COLUMN "user".status    COMMENT     :   COMMENT ON COLUMN public."user".status IS 'Статус';
          public          admin    false    201            B           0    0    COLUMN "user".last_name    COMMENT     X   COMMENT ON COLUMN public."user".last_name IS 'Фамилия пользователя';
          public          admin    false    201            C           0    0    COLUMN "user".first_name    COMMENT     Q   COMMENT ON COLUMN public."user".first_name IS 'Имя пользователя';
          public          admin    false    201            D           0    0    COLUMN "user".second_name    COMMENT     \   COMMENT ON COLUMN public."user".second_name IS 'Отчество пользователя';
          public          admin    false    201            E           0    0    COLUMN "user".created_at    COMMENT     X   COMMENT ON COLUMN public."user".created_at IS 'Дата создания записи';
          public          admin    false    201            F           0    0    COLUMN "user".updated_at    COMMENT     \   COMMENT ON COLUMN public."user".updated_at IS 'Дата обновления записи';
          public          admin    false    201            G           0    0    COLUMN "user".admin    COMMENT     G   COMMENT ON COLUMN public."user".admin IS 'Администратор';
          public          admin    false    201            �            1259    16772    user_division_id_seq    SEQUENCE     }   CREATE SEQUENCE public.user_division_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.user_division_id_seq;
       public          admin    false            �            1259    16738    user_division    TABLE     �   CREATE TABLE public.user_division (
    id_user_division integer DEFAULT nextval('public.user_division_id_seq'::regclass) NOT NULL,
    id_user integer,
    id_division integer
);
 !   DROP TABLE public.user_division;
       public         heap    admin    false    215            H           0    0    TABLE user_division    COMMENT     X   COMMENT ON TABLE public.user_division IS 'Пользователи дивизиона';
          public          admin    false    209            I           0    0 %   COLUMN user_division.id_user_division    COMMENT     f   COMMENT ON COLUMN public.user_division.id_user_division IS 'Идентификатор записи';
          public          admin    false    209            J           0    0    COLUMN user_division.id_user    COMMENT     i   COMMENT ON COLUMN public.user_division.id_user IS 'Идентификатор пользователя';
          public          admin    false    209            K           0    0     COLUMN user_division.id_division    COMMENT     g   COMMENT ON COLUMN public.user_division.id_division IS 'Идентификатор дивизиона';
          public          admin    false    209            �            1259    16449    user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.user_id_seq;
       public          admin    false    201            L           0    0    user_id_seq    SEQUENCE OWNED BY     B   ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id_user;
          public          admin    false    200            V           2604    16488    user id_user    DEFAULT     i   ALTER TABLE ONLY public."user" ALTER COLUMN id_user SET DEFAULT nextval('public.user_id_seq'::regclass);
 =   ALTER TABLE public."user" ALTER COLUMN id_user DROP DEFAULT;
       public          admin    false    200    201    201                      0    16517    company 
   TABLE DATA           ]   COPY public.company (id_company, company_name, created_at, entered, id_division) FROM stdin;
    public          admin    false    202   r                 0    16700    division 
   TABLE DATA           J   COPY public.division (id_division, created_at, division_name) FROM stdin;
    public          admin    false    208   �r                 0    16539    product 
   TABLE DATA           K   COPY public.product (id_product, articul, caption, created_at) FROM stdin;
    public          admin    false    205   �r       
          0    16599    product_realization 
   TABLE DATA           �   COPY public.product_realization (release_date, quantity, id_company, created_at, id_product_realization, id_unit, id_product, price) FROM stdin;
    public          admin    false    207   Mw                 0    16521    source 
   TABLE DATA           ?   COPY public.source (id_source, value, id_division) FROM stdin;
    public          postgres    false    203   ��       	          0    16556    unit 
   TABLE DATA           <   COPY public.unit (id_unit, measure, created_at) FROM stdin;
    public          admin    false    206   &�                 0    16451    user 
   TABLE DATA           �   COPY public."user" (id_user, username, auth_key, password_hash, password_reset_token, email, status, last_name, first_name, second_name, created_at, updated_at, admin) FROM stdin;
    public          admin    false    201   `�                 0    16738    user_division 
   TABLE DATA           O   COPY public.user_division (id_user_division, id_user, id_division) FROM stdin;
    public          admin    false    209   ��       M           0    0    company_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.company_id_seq', 1, false);
          public          admin    false    210            N           0    0    division_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.division_id_seq', 1, false);
          public          admin    false    211            O           0    0 	   id_config    SEQUENCE SET     8   SELECT pg_catalog.setval('public.id_config', 1, false);
          public          postgres    false    204            P           0    0    id_product_realization_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.id_product_realization_seq', 15116, true);
          public          admin    false    213            Q           0    0    product_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.product_id_seq', 1, false);
          public          admin    false    212            R           0    0    unit_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.unit_id_seq', 10, true);
          public          admin    false    214            S           0    0    user_division_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.user_division_id_seq', 1, false);
          public          admin    false    215            T           0    0    user_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.user_id_seq', 1360, true);
          public          admin    false    200            o           2606    16528    source config_pk 
   CONSTRAINT     U   ALTER TABLE ONLY public.source
    ADD CONSTRAINT config_pk PRIMARY KEY (id_source);
 :   ALTER TABLE ONLY public.source DROP CONSTRAINT config_pk;
       public            postgres    false    203            w           2606    16712    division division_pk 
   CONSTRAINT     [   ALTER TABLE ONLY public.division
    ADD CONSTRAINT division_pk PRIMARY KEY (id_division);
 >   ALTER TABLE ONLY public.division DROP CONSTRAINT division_pk;
       public            admin    false    208            m           2606    16538    company id_company_pk 
   CONSTRAINT     [   ALTER TABLE ONLY public.company
    ADD CONSTRAINT id_company_pk PRIMARY KEY (id_company);
 ?   ALTER TABLE ONLY public.company DROP CONSTRAINT id_company_pk;
       public            admin    false    202            q           2606    16552    product id_product_pk 
   CONSTRAINT     [   ALTER TABLE ONLY public.product
    ADD CONSTRAINT id_product_pk PRIMARY KEY (id_product);
 ?   ALTER TABLE ONLY public.product DROP CONSTRAINT id_product_pk;
       public            admin    false    205            s           2606    16575    unit id_unit_pk 
   CONSTRAINT     R   ALTER TABLE ONLY public.unit
    ADD CONSTRAINT id_unit_pk PRIMARY KEY (id_unit);
 9   ALTER TABLE ONLY public.unit DROP CONSTRAINT id_unit_pk;
       public            admin    false    206            u           2606    16684 *   product_realization product_realization_pk 
   CONSTRAINT     |   ALTER TABLE ONLY public.product_realization
    ADD CONSTRAINT product_realization_pk PRIMARY KEY (id_product_realization);
 T   ALTER TABLE ONLY public.product_realization DROP CONSTRAINT product_realization_pk;
       public            admin    false    207            y           2606    16756    user_division user_division_pk 
   CONSTRAINT     j   ALTER TABLE ONLY public.user_division
    ADD CONSTRAINT user_division_pk PRIMARY KEY (id_user_division);
 H   ALTER TABLE ONLY public.user_division DROP CONSTRAINT user_division_pk;
       public            admin    false    209            e           2606    16466    user user_email_key 
   CONSTRAINT     Q   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_email_key UNIQUE (email);
 ?   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_email_key;
       public            admin    false    201            g           2606    16464 "   user user_password_reset_token_key 
   CONSTRAINT     o   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_password_reset_token_key UNIQUE (password_reset_token);
 N   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_password_reset_token_key;
       public            admin    false    201            i           2606    16505    user user_pk 
   CONSTRAINT     Q   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id_user);
 8   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pk;
       public            admin    false    201            k           2606    16462    user user_username_key 
   CONSTRAINT     W   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_username_key UNIQUE (username);
 B   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_username_key;
       public            admin    false    201            z           2606    16733    company company_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_fk FOREIGN KEY (id_division) REFERENCES public.division(id_division);
 <   ALTER TABLE ONLY public.company DROP CONSTRAINT company_fk;
       public          admin    false    2935    202    208            |           2606    16630 *   product_realization product_realization_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_realization
    ADD CONSTRAINT product_realization_fk FOREIGN KEY (id_company) REFERENCES public.company(id_company);
 T   ALTER TABLE ONLY public.product_realization DROP CONSTRAINT product_realization_fk;
       public          admin    false    207    2925    202            }           2606    16723 +   product_realization product_realization_fk4    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_realization
    ADD CONSTRAINT product_realization_fk4 FOREIGN KEY (id_unit) REFERENCES public.unit(id_unit);
 U   ALTER TABLE ONLY public.product_realization DROP CONSTRAINT product_realization_fk4;
       public          admin    false    206    2931    207            ~           2606    16728 +   product_realization product_realization_fk6    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_realization
    ADD CONSTRAINT product_realization_fk6 FOREIGN KEY (id_product) REFERENCES public.product(id_product);
 U   ALTER TABLE ONLY public.product_realization DROP CONSTRAINT product_realization_fk6;
       public          admin    false    2929    205    207            {           2606    16718    source source_fk    FK CONSTRAINT        ALTER TABLE ONLY public.source
    ADD CONSTRAINT source_fk FOREIGN KEY (id_division) REFERENCES public.division(id_division);
 :   ALTER TABLE ONLY public.source DROP CONSTRAINT source_fk;
       public          postgres    false    2935    208    203                       2606    16745    user_division user_division_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_division
    ADD CONSTRAINT user_division_fk FOREIGN KEY (id_user) REFERENCES public."user"(id_user);
 H   ALTER TABLE ONLY public.user_division DROP CONSTRAINT user_division_fk;
       public          admin    false    209    201    2921            �           2606    16750    user_division user_division_fk2    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_division
    ADD CONSTRAINT user_division_fk2 FOREIGN KEY (id_division) REFERENCES public.division(id_division);
 I   ALTER TABLE ONLY public.user_division DROP CONSTRAINT user_division_fk2;
       public          admin    false    209    2935    208               ~   x���1
A�z�s�H~�Ȓ�x(�BY��F���	��L��6J~�y���rl�#/<̯bj�o��F>;E��T[ܟ�<y�x�1���'gk:2��/�����Rf��46�Z��DL}         *   x�3�4202�50�54R0��24�2�估�¼��b���� �;	         g  x��V�NI}�����cM����=��MX%�rA_�l��16l<����?�S5m{��Gb��yj�uN����/��9�P�ᒖ�kJ�z�q�%���qv�hEc|��"{�pbLS�/�J�REs~�:8������@GJ�}�7�j��C�?���`�=Ͳv�Z���0k�����*;�9+=���u�(���>�yE^�<���`?��� ��%V_�U=(��)��b�p L�K��T󊷼L�r0���Z��ng@^f�i
ʇx��	�߾�s�"S�(���<��KyYD��5��x��	�G�E���]�I���k�質Vm��ՠ�G��8{�L8u(2	je}'\^I�(��O���Ӕ��Tr���r��O�U � ��P�~)�όdy0���:��K|n�+���i���J�����RJ�

�0�=�/�|P(edz%qO�z���~֌q	I'P�#E���~�y��}�>-�n"�M3��Iь�Vy%dG���-T?��WKH�nΙ��:v�#mJ�nk#�>�x���<?*�q$�֣"�G�*�<L�&�*a����ڱ���z,�-���2�F)Ĩ�.ܲ���Me3�{�5LX�*	�Q�F�T�Yf���V�j:��m{��m;6������I��w"����~�������v�
���+����hױ�������u�6M�FR���6}�� ���N[I�	��'/���c��hHW��J�ȣg}$z��b9zlk�#7�B7?�^)���`u��'2�^J�����w$����~_�gE����N�m=�$�~V:ti��n���R�=<�]�d/�O4=W�z��e�c�����t���
���**��Lzn�����*�<PћF����8�G�� �Zd\@�{ ���������~����;�) k{�Ljx���8lt�����p=˭h&ܗ|/\_�VR@3������o���l��1{�&�?~���$j��,+�tA�h��G�A{w-\�.a��Ơ�=���d�[eS����`U�+m��r)�p!{���=z3�X�>��Э
��\�0����6_��y���2kA岅�}/���K�yZ�&���Y��j�^�d}      
      x���Y�������g5���Ԝ����qCrn����2˧��[TP���S?�~�K�?������S�?����������S�����S�9�?��O��iu|>����__�|���P��ۋ�ʻ������}���x��<}w�!����|�{�/����%����wY���~���_�C�Ə���S/�(b���|{����מ��~�3zz�]l�~x�!z�wy�{�Bt~���x���_�~:���F�S�����܏?���;|�B�ڏ�9�����������ޙ�~񖓼s����=��΃�y��˟l���S���G��9o��|R�\�}����/*{_�����
��o)��������KH�/�m�E���������ļ��\'{e���bG�?r�w�o��^q��w~�]�o7֒���*�����ݘҾ���(��s��57���X���>��������ykqw�/.���^����E�J�ŵҹ>E��+#�ק������ӄ���Z���.s�;:�������?��m%���ɰ��������������-�aJ�6ƒ��^�J��;�W��{k�y��J�m��+�}v�5v����b��Pr�S/����'SZ_�7�%����Ի�-���Uj�������e������e���rv�R����t�SS��\q ���mBS��N��m,5�ǟ�o���_�;I�ov`��NZ���G<}�݃W����W��K�`�j���療�ۚ�Η��{[[��i`�6�"W��O�����-��]t��̭#����W,�|�]_���m-]���B�š)�B������c+:8�������,��I���ԉۙ�{�8��򃝸��psq>g��lƃ���9�	�x|����{8Kg<x/���v����s���f��-?^�Y{.~���tƾ|��/Di.8?2����CSx�����L�-�\�,���-���YPy幩
^�^mM�B���X��lƃ��WO�k�Y"m��?����w���+ٻ��Pz��td�Z��^x����g����,�\�9���ga�w�&3��D�b��Ç'p*y��Ϊ<��^T��1�����Y�ۙ��{�J N=�{J�f2>����Li[qv�J�����T�k�8�sSKo᝟?����8� ���?��'��ā=�T�c,y?�EW���W����f2c���v�dƂ뽻g2�]ݑ�{f3�w�kl�����K���S[�آ%��S[�ˇN
p�{�0��ZzŘjZrn��x�+�>FI�^���ރߑ��.FIԝ g��fR1��9��<=o�16��/T�U[�:<=pk"�����=p�bԄ��j�j�KG�*ˢ=����xV&��Ϋ�T�UsZ:[_Y��p��j"\���Q��*����ֳ�����<�x����j<�{�7V���z՜6�wӄ�ոZ��_��&D/��ڃǵj�Y����N�F][�K��y��Q�&�D_|f1��K��]��_���:�^]�ג�u�m"*~�x�
����7������{�
��;��'o�����
�Xi�?v��x���=�����P4�=y�≲�{������n��7����?ộ<4��9��:����pn���U6\�=�]�?RO?s�ӟ��;v����Gw(�K.�����sp!wqm/�ݯ�pa���#*;��z!y5>�^���e��w;|\�~��x��9w�x�x߹��z·7s��ˍ�+��L�{,x\m~����.
�ݹ�xg�=�<���W��x�}��8l�xz��L�+�=�����N%���v�q���Bt�ǧ������+;����V���G��T�lT�{�����wv��{�6ܱ�fcyq �^+>���J��9�=�>.r��d�|�����\��➼��_�z6.���/���;/���߫�v���{q<�0{�8��j&���5�/į���B�ո�<uqk�^}�WK������{���R�ð p���h�����;�������z��c<5�j��^������ �ǥz/?���7k39�׽w���T�Y��r�^�<�z�w�m���+����v*�fm�j����N�^�Խ�����=t��y��o�����ٰ�sd5%y��#��������Ɣ�Ib:v�!e�;G�f �#��iL�eF�߫D��Q����m�\p��SO#��y��a8�4{^:_]�����T�:͞��yA�2��5������{
wÝ�4e ���<�aqG琖�gnuq�&r����'������v���z�<�^�����$ZO�)��Qkqou�`X�{s<��Mh�����XN����ݘIN�=��7@n��:Ux
<�m����W^I^�H~�N�x���#C/R�b��;������ O�^7v?/��+qU?x��z"�\�o�éaq�߀��mτ��S�'f� �f^7a!Gm�dF7V����5󜽿h�'�D�4��]h�iT���yu	�K� �������Px��~��$��`r�D�^�̖{7��qOcN�y=�7}���Y�n{���T,�{��XV���m�}w<�1l����4;�����7���i�J����[��4�0����im$�ЀScKI���Z*��T�,��T�D@	8-Z�	���'�2J���O��0�����O��<�ӽ�8Uy����i��H�Y����{[l������������u�7������D��߭~B}�#�����[�$aj7������?�_/����ˤ�w�N�w����/����}q�����~�����_��a\��~��"ޛi��x�p]Y7��={�w6�5�;����&;g�p2w�~���������a�k��I��[�\����[�\�)�[(�RRR�z��JK��[(��St��,΅���{��Ι�5������0�7UQFFb�i��G�{����������r枽s=URZr��&?����C����i�ۓ�%������s)-;`K����-���Zr�W��kJQ����VSZr���ߛ��h|���[j�׊j��j�=�Psv(��o��G�{��l�9EW�����J掔��B�rZ�*�K٢E@��S��B�eW���Έ��.�����9%ߔ��R��)��R2�
�]jrJ@����׎�wq筣����;�+��s-eH���?�NQ���E���������_!��[�����2���+�`��S�[[L4N���� ���&�-���k"�����kN�.�:�"ls��s(�qi��Z�y�AWۚ�ބЄa*�]�lr��w.<�A� �Ž�F]M�ȀS/�&bd��w~�A-:��8u]-��A�¶��{�&~{�NM��J��ݶ����Ԣ�KɉƙQ12�4Bg;�����//9Ҡp]�(/���Ծ�ԇ�KH�4>W6��h-�����;��Gki��v���@S�.s4�O�$}	s��n/������K�\�'k��D.��J��.h�B�e��O��5��"�]��\����Z2�4ߚǙ�j����Ź�׀w*35w<�ܖ�97s����J,�(���؞=g�48W�����~q�ҏ��ج�84\�5e��l�Kσ�i��.�������C�~���s��ra�8�fB�o�Y�e�����:�Kc��ӣ5����ږ�qz��m���\K�������́�M��=
uZmS����i����<ђ�Q��jqG�T��W�.DK� �����+��.�qU	]�k=*s!,��t%t����i��%
C^���%
C���b�����g��fT��ϵ���Yt-�r�>�k���i@�-5N#%-Q����v~zz���
8�BX����X?V;�P�ԇh���Ԣk	�8�d��#43	�����3�؃-Q� \��,.Ԝ�5\�C�\�xg�K;�>���~'h!r��\�s5L��U%-Q��A-W��p�C�����_[ͩ�C���    \�sqE�?��3ǵ%*b@5;���̅�(p!s�����vЀ����ZCC8-*I㧒���d.g��B��7��h��ନ�Ҏ̜4�o48o�d'����Usw>���X4|O�e �jn� �X$�Hp~�E�sw��jn��q*Y�#h���:�A�Ԛ��L��Dhj���+\ܑ�K�\����>g��̘�/N#�=Q �Zs}��q����#�7�r=QY �f!z�6 8�(��P�MO֞( N�s=�����LC��z����=Q �Ƹ�:͵�Qz��-(����,Nbio߹�+a�4M|�Dv8u �	tx�.�\���<	ѓ�����\_�1и�sqp�@��G4�ָ8��4M|��G4N��=��΢����h�k����}Xg�H�
���!
��0��#�hͼ�up��ŹZüp��ଞ��޶Ӣ���S��'����č��VZ��^�Ѹ�����t�~�Hp�~��J��
*�J�p��jh�WZ.L�'k�\��qz�0�Zh�8�@����J�Žթ�Yܑ:���㴵u؁��4��r��N�^ː���{?�i��p�@S�����b$<7д"}$�6 ��'W�a���܀��Xn��8s �2�Y�TύD~8uZ�q�����Hx?��aqg�x�Hd��S�u$�'�B�žp���?;MA�9Rs�H�0ޏ�+x�H�^��1�ُ����<N3�c�PD����{;��ˮβ�#�>�\��5hj�{���s5�yA�.�a��`>����B����i��H$+_��A�D��Psq�2p!rIZ�\"/MK�,��8-���,�5h�1�����ISC\�\XJ�����Nk��8�͍�Ri�7��ǹR�h���s"v�@������S=w$|/�4Xr$����P�;��i��H$z�3=w$��A3=w$�N�T�,�U����V�{����_<�q�^�Ž�F�^��iH���o/�m=��5M!,�-N��G������	q�qLy���G�cNs�G"U�Ƅ�8=\�DI;pV|$*�_�*5�[��A	�8KC��R��G����4Rr$����9�;2W�=w$B���z$Z؁��ܱA�4uZ�D-���aL���8?�c:L���&�\g��\	8�Y���ξ�$D'k܇ �1�Fଈ�0�u����w�x�k	�@�0I��G"���j��ଈ�H�@��a_*pq��0\�%,�N'1��4|$fs�8�A�ٚ�i��H׀s�!�- M�^G��8�V ����� �D+ p��z$:�A�đ��>kr���ؾ�x8&p%s9}�@ɑ�]�{�������A�5�����F��D�'k܆ ���d|�w�Nb:�1>��L9NO�<N��g6�sR����og�Ù��?^�� ,�N+J��:l�S�L6�8���|��8əgG���ř�`inS��a�3�
 �e�,��e�4lq�#�S��L�x�Sc�L� �'�((y�sy�:wv�%g"��V����p�i=7y �ks?�ir�41G�yę�y \�,N���D���������i��4q����J,�?��#�,N�s��皮�)r�4Rr&F�.��NsgbZp>�</=S��y@EI�D.u��|ә�1gęh -̹8��FJ��s����g� �����y�*$.,��$.�� �esg�{�����Prq�8Wr5L!���iM��(bN%y\�sqʋ�ę�!N�^g����x�$�Ə�xL"p��1&�4	q&꿀s�5n���.�f\	]��I�3�zq~!ę�1��9�;�7BX�{x�bx&j�@�$ęQ�.D\>\�\��B�Z�d�O֖r �Φ�[�1cgb�x�pZ�t%ꯀS���1��@ɕ�"MO�+�[��9��7�i8��7�	���X���L�,�|-�	�J�0g��+޺����o<�p%c<��$.gCӛ ,����4�h9N���-�Y5ɕ�
���DӋ�Ԕ����'_�[n��˹<��i����z�W��8;V�D�h�u���!w%拜/N3�i����(.D..�N�$Wb>	p�沫� ɕm��j	�F�i�JT0�q�+^��.<_�S��JLyq�q%�S���+1J���J8H	8�岴8Y��pZ�t���q�h��40w%Z�Ӭו�A�8[� �����L@l;��a�+1e�Pt�% �������i�+Q���I��0V�B���p�^&�$���7y\�q�ދ��� p'�#I�U�P��s�	�xO�����ҹ��:�7�����<�����4�z%�<�Y�J"�4�B\�6;�4q%"-�i�ܕ�N� �D�	83��Dhj��&��H,�B܉"*�̍�\�i��N�C�$�N�^�OcJ/�ht�ND���)�w^���N��� �DEp%sa�8u#�D�8Usw��NM�;����w��k�N}�;18s#��_ev�wB�9����Ž�iY��(N N�w�� �!>L�?��)a�h������,&|'�8^�_��iӝp����;18�����������sw�"ഌ)�Ө��΢�w�� 4�!�����N�_��=w'�� .�x�
p���&�4^r'�����.�8I��q���ם�	�[tq�K��q�m �Y��N$�AS�NxO�iQɝ��8���	��8[�9%�I*�|&A�|�{�3{n����݃�����w6��"�ًK<��g!��~;3��Q54�g���<�ȴ��꤮d���cM_�g�S?x���\	]X� �$#:�P M*������\����܃�,�t��;��=x\�r��t�(��������9�H�gy��~:��,�>�ME<xxep�E,�VT�Cׅ�k��X�5�<x�x�¼��_h7ă���Bo�^pouq����̋x�P
�M_hG�)�{�0Z�q�8��/��˅A߸���b�O� �� �h%r)��v�w4���S�[������� ݂;RC�!���*s\�x�t$S�����_{����� ��,.�.��m.4�9�,Ϳ���.]�D��q�hq��A���l�k�s=w��f����.D�'k\a \H\��Ak.���p!��"VG����ׂ;�}&"���=��Z��:��Z�>�,&��a,�����8KD,���,>W6�$���9K'2-87ǀS-W��y��i��<��S5W1&�lpă�5���Y�[��J,�m=ZK�B8K��gt�k�Ss�$�I^��D<x���֒�2���\I�y����$5�iH�$bT�Y���;R�$���9W��B����o-�0ы�D����Y%�i��$B%�Y���;��׃��$�b���C,���J��ft��o��4(\��WB����X��K}A��5��^��aAh�������t�MC�%*����&4K�/���iH8���W1����C�sy����*���a����ʮ�Xh���G��Ņ9�Ņ�˅*�b����<Q��s.�z��E�s�`G-B�R�ZH��B;����WI�r .D\L�%!J�4KB�ĕI�OnS�Z��I��CGJ�ݮ�s%�y��@I�c�\X4q�l��4\�h��i<����O�s���f�k\�3q�l��ȍIS[�����S-W�{���Ģ�k<5c��k���6q��j\O3i)���>q%sQLw�z������8��X����ėŝ��g!ҫ��!�tx}ͤ��ji�{����8�L���p�������j�u��i�����4PR�d&.�\��B�eq��B�{�ԅ��ω��5�C�8��j܌1q����;qZ�Y�N��E�]]I]Τ�_<l%~��։�V�w�`�K;2��5�Je��t�tN\h���a�,VR��I��\�G(L�����ٔ��Yt�v�wv��fZp�ц�w~    �)4]x�����&Nk�j\\1q~��B'��.r�'��Jj<aҤ�s�,�B�iq�fq%q9/�]vuq����!����p"�ڊ���5��Y�ߠ3q�D���o�����^D��4i�k����B֕L\����w����J<{b�{��{�m���d6ӆ{�)����6����N���5݆���H"bý���S�[#��SP#�S�	W2��'��������w'bý��:����4o�|�օ�=ߺ�\Y �K����x�����ɿ������̐��;�&.�.� �.���;[�Of��;�C�^X������s�K��~�n�����u�{�0�0T|O�n���L�CM���ah2�	x4�`�{x�.]��#M|Ϲn���<4Ł��5샙��{�8T\�tY\�Y|�����ӅЅ�,B������q���tq���,.��8�|w^��t�"bÝ�����vaӅ�� K,&���o���c�Ω*�� ��M/^};qq��u
���Q5��Ŏ7p!s��ʉ�K�N�"��,S�|���v�{�dÝ��Ɉ�.���HOğp��l��#��^D<t}��A:�9Ms�A�����5����LF,�)� NO�w5L��5�K�8Ut5�>�B��#� ���O���Ɓ��K:A��w����t�R������<h�֬E�"6�{v�������~i.���I�UtZ�z�5.͟�VE�'��5��0�=<t�^�j..R N��{Iӟp��=��S���$p%sa|8u\3E
��=W�q����ŝ��Z.N�?t����/�{mɆk]�?�q�qq�Ĺ̅���2�ڜ��tZT����o��\D~u������S"��~��,-��𞅉3��Ƶ�����	�����iNS-�4N����)�lm	�4�D���,(܌��l\e�W�4���i����ʜ8u Z�:8��Z<G`�T˵�A ���Ž�.$.�8MC���
|��pGhfb߹�)ڛ��0��FJ򫳓���BNZ�\ʒ��X���~��qI�z��8���@:"^<�ց�Cr6�yk�j��g!,�(��h��Ɖ�Kd{��8�Ž�΅.�8uZ�	T8B׷)9��@��t* MM�ϊN�����\&�9K��pG`��Z2R��6NtN�!p9K�!6�8r=Ć{��z-��!�T_�k�M\�q���rqKp�k�~Н|x���i֫%z�-W�p&���ۅЅ7kL\]�b'�C�	�z��!����c���W�'kÒ������փ��q�4?Yi��@�5�.�ָ>���U����깞h�N�V�;[w1�'�i�\��N�깞5 ��kO$z�S���{o4����H�8M�Z��y����J�r�ꦇ�Ž�i�$�:3�z�sM��z�8=\{"I�ƃ{|��ĩ��2a�,8gi������v.���9�n|7���<��NDg�K{;'d���I�v��w:N\(�p��ę����=�Y��)���1�D.�h�LA�k؉���&o�����R��ȵ�k\��	�D�2p�D�D+=pq���2�Bх#�'N_=w?q�E�q�E�D�8?\c�4M��D?9p�l�	�8�"z���d.Lr�E%=�;��]\�֔�Dy4pa�����ȅ=k�i��'����!z"�\(���;K}��8��a�p�v��:���=�\��qm7p�������Ԟ�W�̞��c�'M�V��_���_Q7q�C����깑(�N��a2��oLύD{+h��	�8��D�0p��Fb�p��F�uNt#��:#� o�B�J���9��H����H�<_����;��fK;?�g"F"g	�KF��8m��l/pz�Z��b��L��W�;�aqoq�Zړz�Z��w��Y�f��?��W��t��`����_��z/}�e�h��k��_����}�࿣+�ÿB�����E�e	�j�{g,��R�u��i�ۻW꾞L-/��ū3��đ� ��W�l��	��4��{z�#��&ޝ��k\Iޕ�<%xWNp����������ANb������E�|Rr���a�GN��o?�	�h���sJ9����sR�Ml�9'5�ś��lh~p���o�!8Gn��vY����}7�K�wwf^�1u��㓸8kKJ�HL�93�w 1�+��0��描��+���?�m�Ѹ<;�A�'��_�9/$'�wgn�Вw =��~-�y =���������и��~����!y6X��.�>e�Eh��X�sT��M�?ū�Ww���ƪ5���ݫ���ŝW����qs GA��H)�(����x�@?��v��s�pe��� ͟⳿��\x��on��Ɯ�՛Ryw�������ߜ)�����-�r��?�{�O%ώ��p�����Rɵ�k-���].��f۷���#c�+v��#|�X��?r�ש�����G�b�~;`��ޫK�_��ֳ:�O�+�3�'n]��ħs�>�N��e@���ݮ7�h��={�4�R�����[��4�R� �Ƈ�k���5�8un��%�>�s��W�h�#u�k�t�q%v=��#wA^\ru���t�w��#%u���#�:u0�2S�'W�Y\����J��������3%�'���k`4^��%q���㝹�y|��T�e��O�z�@��M�ؾ)�����I�z�/v��:5O�uƋ�k��������w��r���_l�[{��;���K	_ξ����sޥt^�5��s�{A	��X/��8�-֡!�J���p6�?	�ᶴg�v_�x�i|����x��S�gq���Lp�,�5��[�=}� �����.OO�u�Ʃ���;��8��y��_|����JN}�u&�ƩƳm�\a�y��0�_�(��	��x�оN�ڵ1_�4akq���S+�-��4N����_㇐��N�fw<![�{z��jMx�*�.��Q<�3@(�S�7.95�' �!��wh��xv8���W���x��;�[]�&��h�J�fm�(�F��<.^��Nӵm���8-�jƸ�pa�դ�lJ���/pa�ո2
��0x�VF��k\�v�yڢ-}�*��Ϋ�4i�_��*��O��w����XսR��|*zvN�cavȳ���w*y=��O+T��>������-Q���y��<;����"q��G����Y�Д�:�@�<����>����n�q^�ғ��9�k�-8�3���߹�?��%=c�����/NC*��VH��qD�B�JX\\���B�ӐJOd̀S�v���q�a�񓇑-ｺ���7/������ �<-S��$/V��i!�x�e�s4.���'�'>x�f�D�
8m��Ћ��{�w�8�~ �� �'�[�4��3)����˜|�Ӏ�:B@�4}���8�`Xܓ�KH^���c���M/~��JO���y`�'�*�c���pqܚ쓃K���p�f�[_Iޕ�[�v��iH�����:��\Ю��C�B�uҴ�_<�1�]���4�gy��\z��qz��a�ùx��λ�>��X�)h��^��/&:v����h��S��}�a=��/�*J�K�4o�<���B�z�,
����X��k\�^��?�(��<�X	j���c�pN�~,��%�{0F�7N}۱��8m��� <�[�؅(�4}���8�m�q콽�鳱�"�8m���9x��8��5N�ۑ�5}�F���Șr��i%�H��o\ח�����8�|���L�Ә�X��k�V��:r��A��k�&�]�-���8g�9p����5x�[x.z���K�O���UO�*��\輸08���Da%p!z5�  .T^r�A��G��8��Yܓ�!L��������iDe$c��l,�	5N�ڑ�;�q䱌��8���fG�ԫM�0��1p���'�*ܺ,/����ӣ��.�#w=k�D�#����#�    ��>�Ź��'�,�v�#SO��'�iqd:-�S�������#����c��M�ԧ=N)p�\�O��;��[	�Zy�2�P�4sfq�;i�ʑ)F~��z��6p%wI#�'/��ӳ�Ht���T��k��S��g	�&ΎeС��a{��{zO9�s�N�w������𷐼�^�S����o��,���q��9�nZ�|$����NG�ˠ|���#ѫ��w�������C�ӪP���5xn�I�����#��߼���/̼F�+���i������y��0N����ő(��Bˣҫ���:x�⬭qa%xZ)p$cw�C�L�*xa�%f����#Q�\��ɠ�-&H�-��W��rP�d/n�O'&����Ņ�����F<�t���@�V��b�׹q1=p��j�yN�G"$\x���o�6ﰕ
\����̔���yg&����;3��S�����n���=�w��?�=��?�
ߙpo���4�#��7y�v���������4�S/�LxX���;m��{z��,�-�ܽ=����N݌3S
�{�r��i����r�S��LT����LU���tfm�B�%}���;m��[��V΄�\�^v����D��P|�Rg����:3u�๽w.��i�F�Ok�9��^�31x??)�-=3��ੳqfm��vݞY��P��t��uR�m��xq��-|���pzx!{�������՗�i�ZnI�8�\�TpZz%RQ�i��⎹ȳW\�x�_�f3,η~�4�w�D}��Sg#�_�,��\�L��X���ҳ���b�ԕp��)\�Sɛ8�4򸒼�Zj�J2�8�r/�����<�;?��Q�ey绹��E�ĩ�{�^��i��{i����\��q�f\��s��M��Z���o!xap�B�J����q����+T^�"N��y�>�ą����ʻb�t�4�gq��<-ѳ��/@K��x���iD���K'NK��x���i��/4{{�9��Ӫ�+1�i�8��α�Ә޵\��qZz%Z>'O��]	�x�Խ����i�Z��ָ�x�@���~�+�9
�R�����}�r۷ƅ��Y���Ƴ+��L��\qa��i
-�+����'N#*�rٸƹSky﫫⠭�h�M�x5���<�j�����X��e�4�gy��+�K�J�rgeGm���8��[�'O���f9�]S/�5?�����0�Znl�8����Γ��RW<�w�4�v������k��\�B�eqɻL(��Y|���tO�� Yܑ[އq�8��S������cfq�z�Z�S�"��|j�����r���<�k�<O�܉���IR�	�9���^��O�J��9f��ۄ�����#C-.&xz�.<�|�J������w\�?y��މ���S�wǍ������Y��'�lӫ��J�;.��8��Z\��i$��N�C�M|�J�~[�g�OP�8u0�x4�ą��q@����C~��6l��8u.�D8��Y��:>D��»h��6�0g�y��N4ZO�f.�D(8�=�M0�
��sq'
E&/�]�(�Vgmk���ǃ�S��ﵝ8Ϛ����� �Ž���ŝ�l�<�l�[�6z߉�pZ$u���f^8���G�^藃�mf^¯OC*�e�|h��ە'�D/�FXb3qZ�gq��+�S���gkq���+��Yy�,[��a 8�ZxOp�����m��x~V��s�ݵ��a���Y���k�U
�}���Q
$/�#{�>�?�J���։�0�;��6b<x���$/�ri'��G�cg�ނ;�G'I��-5ppV��aD	8-�_x糥wy/���kz�~�̳]pg���,��ۙ���Q���|qv�.8x������}�8;j|D[��xν��ɋ]�c�<��V����\ۅ�!�,���s�y���8+D~������w�<m�Xpw��9��s �B*_>n�?�����_����ۅw>���T���������8����?��gA��n��8�<x4Dk�y���Ex����h=K�6.��sj�P�g|������9[���kaE(pS�.D/Κg�Q�d��Y<o��i�ł;?�d���,�y��{R�6�:=mK<�z��b�����s��Y���û�&�
B>jR�8�.J��������DGI=xOU^I$ހS���s�g����+�z��o����D<����C2y�ܖD;p�n�������Xx���Ի-Y�ޭŝ_�+a=pV���aH	8��J���0�FUJ��8+UYpWrX(y��T��x�@4��	�}y��b�8qK	Κ\�z��ȍ�M��nW��id�d\D�>o彟O]\�{�OC�%
��7�O����g���pL.��MW�y�1�b<L�+4�R.p��(�������xh��.l7|�J+%�	4q%vaX
8MaX�S9�	����!p��]pGnx����Q����q'��>�*����<��f��;���Z��#���J��q	xz��D�:pz��L�<���O�=[�����q�qi)pz�Z�}�4�by��ӀrM\��^���Χ�i��WM�����R����R���8�Ns���S�w�$�S����{x���I������'p���+�&�:1<�N����?�d�&җ���aqOe�9R�8���>x���m���M��1,�)�C(�D�3x���Fk�4�V�s���Ы��T����N��?x�N=ۚ��;y!xq�5p�n�����xq���6�G� �i���>�Ey\	^�����m�d���<FM�/�<����ap�b�0|�c<٫xz���r�w��C*�c�^���Ϋ'b 3X�w�bÝ���;o��0������g㽽�5ކ{o���&�I �`��3���7�y����M��IM��;��d0��'=�=�<�b���/aL��x�������!#���Is��I<��>|�_������2)�qL��]�C
��ʋ�O~?l����FfH�	�KU��>1����po�ݩ�p��4�� �k�k���F~�8��TI��n�����p��ƋS?��`
����n�s�62Cj㽧�k�6��h�)�a��D
x4�x%Ad�aH��x�p��+��3��I  ���m��ퟰ�j�'�yo�#<u�/�������ŉ��q�r�=#�xI��5j�Y�ٻ��<n����e�h���Z���qG�� ������ʆK�^"�����sg�C���5� �<�
c����O
���c[3�c���P�q�
x�����:����N�[�>��=nm����N}ۚpP��%R���摻0��Ӡ�� 8�G.��po��^���\������:���|����f���؇�l�#xcO�ϩ<6Fj�=�;��Нwv��a켃��g��"!�{�l��_O�*5�߁WJ/��S;�f�܂���{�S;��� ?�s[sj��U�����?��l�po�O!yq]#p!x�o\����m��kr���{O�5Q���"�DU#p�^�7N|/��p�!s������0�\�Y|���p�#�y�YK�m�6�Q�"yay�F��w�L��Y�汼�)���K�����T�ڿ��+�T�,�*7���3tX��{?�F�j=yϳ���^��/�����J�%n�W�l�~��-�y��-ϕ���4�ܬs�7�^-��\��j�%��4�aq.x����0_|�xXO��E(�xp �~ù���E3潳��x-��*/�=������|/D~�ظN�Z�{o��S�8uk[�i��I���H�^��N���iOS�-1�8��6[����k��x��h��.�B��w �^mK�NSgwf���6�{��F�-Ｚ&��_�јMXy5�3��H��;�O�H��#y�T��|y�'���Z��S�����ో��f�,�|w�*���=�V^�����n���i�m��[����x�!p�`X����i�T7����Ի� �  ���o����!_���y��EOL� N��xG���Z ���'g�����l�~��Y�fP��c��+��vP����8�9���q��`^OԦ�������Y�����������K�B�6:xE���<=k{�}8-������;�wҴYO\4�F�{�7�4m��'��������4�J���4���i<%�S�g��{�����Ƌ�2 ���������NKT�8�P��5spZ�3��}X(���<��ꉹ�����ɋ���H�O�S���w��oaqg�y�b����d�;g�H^��x���<l�N5�ŝ��&�����6j$2/>>J�R� 7` �A����������A.��x��Ƈ�#Q�|���O8��L�xz�^���=�����Z헷�v�G����ܜ�����}�f����*V7�Q%-���_�k���g�7���_�k������"�_/���0	���׾���?�Ē��g/�8�z����-�둽 �q�5^b�t��?[Шq!x%{�]�~��<R�Wc]r�Bp��o?���9�Cꂈ�	i��?~��L�Mȝ��q����V��k�A�b۬���խI�9���Y���*����w������O.;u��z�.v�7�����x�#:0^�+�;SO��y��^�qe/
�x�pq�6[���|K�v�*�e���"u�J�6�N�T���9��S�[��4����ݯ�4Nۺ��xeﾦ-�.��+=�S�W���Sѫko��.:%����X���A�w��9��ů?r���Ūv����FMX+������ N=�s�������wv���f��"r?�����ys�z/��ZögK�*���8���Y>�y���eqj�ե@G���]��5.4�R!�yj�-���"}A���6�K,�SH_ɹs���ź{m	k���"t��X�-w�ƴF[ǣj�꼶^�y���R�qj�u���O�}�{�bZ��q{�w3��Ƒ�؟~iּ��R��M�*-���ލԗ�����Թ]J�����Z�-7@h��U�15�7O���	ˀ���Y�|h �-c65^��=\(=����=>B�j�-O��<>�W�3�0����i �AVo�;>4�g���8fc�#��3���d����җO`���wT���w�Ƒ�Jf��@��$�������N�m�Ѽ����'����QkK�5N#ɶ��{��R��������3��:��O����]�^d)L|�͋2(�����Y�4�gq*��gm_n и�(|f0������a�0�'_ŏ�s_m�Ѽ�0�'��Qߗ>M�S+o)%��F�l)������%����i����ϧf��E��~��m�c�/\�˙z�N���\�qz��eҥƩ��ǩ�gqw���2k�����Ӏ�R��~������'1�z˛��y[�_ޠ	�Gc'.���q��w�����Ӹ�R���3��}��J��CH��6�������Ә����=����Kq��S?c-��8��,���Lc,�n��s���;g�AK���L�Sw�uD���d[��n��l}%{�ũ�;�;�%�S�D�y�ihe,�W�����B��T�Sko,35N3c�2I����Xsp�f,���܅ef����S�_4���im,S�4N+��2UC�J�B'	8Mb�r^�%�]+&O�ڱt�h���qW�R�������	�M�*#N^N\��Z��iDo���a����8��͞��q�ci"�8=i{�;*�ٳ#ch��>�aNzOt��;��]
??�V�H�>'O��ci��8Uyw6��Т��Dv��{{,-�����ݧ�g�a;)\H^�-|�B������J��Zm�3����#V/D/��3�;�����*y�������ؼ��e~�wv���iD�q���ii�xډq,�xH�
�הO��y�:J�B�υ<�'������婩w��ʚj��v��zy�F�T�_xN>�S���z�:KI��3����c�D㍯��TpU93���D�4������,�}��FU�e���/&��rϟ�iyh�y�3�N+�OQr�'2Ό�	��%��-��LpOkky�G�[_�^	�$�������S�wZc��앚�:c�����������N��            x�3�44Ѓ K�?�=... (��      	   *   x�34��q�I�����P��T��X�������Ԓ+F��� ���         q  x�}��N�@��ۧ���"'Yi
�(!!X(P>,����p�b���ɓ`�D��0�F.&j2�d�3����Z���#G>V�j$�%�E��%�j�i��D���((^q4h���E7/�e;fs@�!틲�}��,j�#S�w�Z��5ݳͣ��e�_�O� xdcX�l`��V��w.-��=/���װd7���H��8�CX`)�|�oڹǈ>F�d�D�rr6?���|U�F�H��0EH�����Iv�^׸*%mK�J�J[ʩ�[�r����@-Ӕ?��H#Y#����_<�ٞ���rN�d��r�n�u��&�r�f`�o��M�l̥9l����)���_5G��A�+��            x������ � �     